
; (load "C:\\work\\gates-as-built\\scripts\\regionFunctions.lsp")
; (load "C:\\work\\gates-as-built\\scripts\\variantArrayConversion.lsp")
; (load "C:\\work\\gates-as-built\\scripts\\polygonRanking.lsp")
;; the above will already have been loaded by loader.scr

;;it is really sloppy, but I am hardcoding inch and foot here:
(setq inch 1)
(setq foot (* inch 12))

;;gets the value at the specified key in an association list.
;; aList can be either an association list, or a symbol, which is expected to evaluate to an association list
;;for some reason, passing a symbol argument for aList is not working.

;;this function takes a string and returns a version of that string that is guaranteed not to have any characters that are illegal for autocad names.
(defun sanitizeName (x /
	illegalCharacters
	replacementCharacters
	sanitizedString
	)
	(setq illegalCharacters 
		(list
			"<"
			">"
			"/"
			"\\"
			"\""
			":"
			";"
			"?"
			"*"
			"|"
			","
			"="
			"`"
		)
	)
	(setq replacementCharacters 
		(mapcar '(lambda (x) "-")  illegalCharacters)
	)
	(setq sanitizedString 
		(vl-string-translate (apply 'strcat illegalCharacters) (apply 'strcat replacementCharacters) x)
	)
	sanitizedString
)
(defun getItem (  aList key /
	aListInternal
	)
	
	; (princ "(type aList): ")(princ (type aList))(princ "\n")
	; (princ "(= (type aList) 'LIST): ")(princ (= (type aList) 'LIST))(princ "\n")
	; (princ "(= (type aList) 'SYM): ")(princ (= (type aList) 'SYM))(princ "\n")
	; (princ "(not aList): ")(princ (not aList))(princ "\n")
	
	;;I first tried doing the fololowing determination of whether aList is a symbol or a list using the (cond ) function, but cond did not work the way I expected -- I think the interpreter must have an unusual order of evaluation for arguments to cond.
	;; using plain old (if ) statements does work as expected.
	(if 
		(and 
			(not aListInternal)
			(or (not aList) (= (type aList) 'LIST))
		)
		(progn
			; (princ "it's a list\n")
			; (princ "(or (not aList) (= (type aList) 'LIST)): ")(princ (or (not aList) (= (type aList) 'LIST)))(princ "\n")
			; (princ)
			(setq aListInternal aList)
		)
	)
	
	(if 
		(and 
			(not aListInternal)
			(= (type aList) 'SYM)
		)
		(progn
			; (princ "it's a symbol\n")
			(setq aListInternal (eval aList))
		)
	)
	
	;;at this point, we know (more or less confidently) that aListInternal is an association list.
	(if aListInternal
		(cdr (assoc key aListInternal))
	)

)

;;sets the value at the specified key in an association list.

(defun setAttribute
	(
		theBlockReference
		tag
		newValue
		/
		theBlockReference
		attributeReferences
		theAttributeReference
	)
	; (setq theBlockReference
		; (vlax-ename->vla-object 
			; (car (entsel "select a block reference"))
		; )
	; )
	(setq attributeReferences
		(gc:VariantToLispData 
			(vla-GetAttributes theBlockReference)
		)
	)
	(foreach x attributeReferences
		(if (= tag (vla-get-TagString x))
			(setq theAttributeReference x)
		)
	)
	
	; (princ "\n")
	; (princ "theAttributeReference.TextString: ")(princ (vla-get-TextString theAttributeReference))(princ "\n")
	; (princ "theAttributeReference.MTextAttributeContent: ")(princ (vla-get-MTextAttributeContent theAttributeReference))(princ "\n")
	;(vlax-dump-object theAttributeReference)
	(vla-put-TextString theAttributeReference newValue)
	
	theAttributeReference	
)
;===========
;; aList is expected to be a symbol, which is expected to evaluate to an association list.  we will return newValue
(defun setItem (aList key newValue /
	existingEntry
	updatedEntry
	)
	(set aList
		(append 
			(vl-remove (assoc key (eval aList)) (eval aList)) ;;this removes any exising dotted pair with key as the car.
			(list (cons key newValue))
		)
	)
	newValue
)

(defun writeTo 
	(
		pathOfFile
		x
		/
		file
	)
	(setq file (open pathOfFile "a"))
	(princ x file)
	(close file)
	(princ)
)

(defun writeLineTo
	(
		pathOfFile 
		x
	)
	(writeTo pathOfFile x)(writeTo pathOfFile "\n")
	(princ)
)

(defun writeToLog 
	(x)
	(if pathOfLogFile
		(progn 
			(writeTo pathOfLogFile x)
		)
		(progn
			(princ x)
		)
	)
	x
)

;; almost the same as writeToLog, but appends a newline.
(defun writeLineToLog
	(x)
	(writeLineTo pathOfLogFile x)
)


(defun appendTo 
	(
		theList
		theElementToAppend
		/
		returnValue
	)
	(COND
		(
			(= (type theList) 'SYM)
			(progn
				(if (not (eval theList)) (set theList (list))) ; if theList is undefined, set it to an empty list (I realize that this is a tautology in lisp, since an undefined variabel has value nil, which is the same as an empty list. -- oh well)
				(set theList
					(append
						(eval theList)
						(list theElementToAppend)
					)
				)
				(setq returnValue (eval theList))
			)
		)
		(
			(= (type theList) 'LIST)
			(progn
				(setq returnValue 						
					(append
						(eval theList)
						(list theElementToAppend)
					)
				)
			)
		)
	)
	returnValue
)





;; this plusPlus function behaves like the postfix '++' operator in C++ and similar languages.  That is, it returns the initial value of x (the value Before incrementing).
(defun plusPlus (x / initialValue returnValue)
	(if (= (type x) 'SYM)
		(progn
			(setq returnValue (eval x))
			(set x (+ 1 (eval x)))
		)
		(progn
			(setq returnValue x) ;; in the case where x is not a symbol, we will return x.  This is just to be thorough - I only intend to use plusPlus x to operate on symbols.
		)
	)
	returnValue
)

;; generates the list (0 1 2 3 ... (rangeSize - 1))
(defun range (rangeSize /
	i
	returnValue
	)
	(setq returnValue (list))
	(setq i 0)
	(while (< i rangeSize) 
		(setq returnValue (append returnValue (list i)))
		;(setq i (+ 1 i))
		(plusPlus 'i)
	)
	returnValue
)

;; creates a new AcCmColor object.  argument is expected to be a list of one of the following forms
;; (r g b), where r, g and b are numbers in the range [0,255]. (each will be rounded to nearest integer.)
;; ... more forms to be implemented later.
(defun newColor (argument /
		returnValue
	)
	(setq returnValue (vla-getinterfaceobject (vlax-get-acad-object) (strcat "autocad.accmcolor." (substr (getvar 'acadver) 1 2))))
	(vla-setRGB returnValue 
		(nth 0 argument) ;;red
		(nth 1 argument) ;;green
		(nth 2 argument) ;;blue
	)
	returnValue	
)



;; Get Document Object  -  Lee Mac
;; Retrieves the VLA Document Object for the supplied filename.
;; The Document Object may be present in the Documents collection, or obtained through ObjectDBX.
;; It is the callers responsibility to release such object.

(defun LM:GetDocumentObject ( dwg / app dbx dwl vrs )
    (cond
        (   (not (setq dwg (findfile dwg))) (progn (princ "returning nil\n") nil))
        (   
			;this evaluates to the document object of the specified drawing iff. that drawing is open in autocad, else nil.
			(cdr
                (assoc (strcase dwg)
                    
					;this evaluates to a dotted list whose elements are of the form (<documentName> . <documentObject>)
					(vlax-for doc (vla-get-documents (setq app (vlax-get-acad-object)))
                        (setq dwl (cons (cons (strcase (vla-get-fullname doc)) doc) dwl))
                    )
                )
            )
        )
        (   (progn
                (setq dbx
                    (vl-catch-all-apply 'vla-getinterfaceobject
                        (list app
                            (if (< (setq vrs (atoi (getvar 'acadver))) 16)
                                "objectdbx.axdbdocument" (strcat "objectdbx.axdbdocument." (itoa vrs))
                            )
                        )
                    )
                )
                (or (null dbx) (vl-catch-all-error-p dbx))
            )
            (prompt "\nUnable to interface with ObjectDBX.")
        )
        (   (not (vl-catch-all-error-p (vl-catch-all-apply 'vla-open (list dbx dwg))))
            dbx
        )
    )
)





;; explodes the blockReference and all nested blockReferences.  Returns an autolisp list of the resulting entities.
(defun fullyExplodeBlockReference 
	(
		blockReference
		/
		explosionProducts
		entity
		blockReferencesToDelete
		blockReferenceToDelete
		moreExplosionProducts
		i
		result
	)
	(setq explosionProducts (gc:VariantToLispData (vla-Explode blockReference)))
	(setq moreExplosionProducts (list ))
	(setq blockReferencesToDelete (list ))
	;;(princ "(length explosionProducts): ")(princ (length explosionProducts))(princ "\n")

	(foreach entity explosionProducts
		;;(setq result entityObjectName ;;todo detect deleted object by catching the exception that happens when we lookup ObjectName.
		(if 
			(and
				(not (vlax-erased-p entity))
				(= "AcDbBlockReference" (vla-get-ObjectName entity))
			)
			(progn
				(setq moreExplosionProducts
					(append
						moreExplosionProducts
						(fullyExplodeBlockReference entity)
					)
				)
				(vla-Delete entity) ;delete the blockReference that we just exploded.
			)
		)
	)
	(setq explosionProducts
		(append explosionProducts moreExplosionProducts)
	)
	
	;;filter out any erased entities from explosionProducts (such as the block references that I might have deleted abov, and other entites that become erased when they are exploded out of a block reference (dimensional constraints, for instance)
	(setq
		explosionProducts
		(vl-remove-if
			'vlax-erased-p
			explosionProducts
		)
	)
	explosionProducts
)



(defun recursivelyExplodeAllBlockReferencesWithin
	(
		blockDefinition
		/
		entity
		blockReferencesToBeExploded
		blockReferenceToBeExploded
	)
	;; explode all block references
	(setq blockReferencesToBeExploded (list ))
	(vlax-for entity blockDefinition 
		(if
			(= "AcDbBlockReference" (vla-get-ObjectName entity))
			(setq blockReferencesToBeExploded (cons entity blockReferencesToBeExploded))
		)
	)
	;; to-do: add the ability to specify a particular block definition to use as the starting point, rather than always assuming that modelSpace will be the starting point.
	(foreach  blockReferenceToBeExploded blockReferencesToBeExploded
		(fullyExplodeBlockReference blockReferenceToBeExploded)
		(if (not (vlax-erased-p blockReferenceToBeExploded)) (vla-Delete blockReferenceToBeExploded))
	)
	(princ)
		
		
	
)


(defun objectsHaveSameHandle
	(a b)
	(= (vla-get-Handle a) (vla-get-Handle b))
)



;; thanks to https://www.theswamp.org/index.php?topic=41820.0
(defun GUID  (/ tl g)
  (if (setq tl (vlax-get-or-create-object "Scriptlet.TypeLib"))
    (progn (setq g (vlax-get tl 'Guid)) (vlax-release-object tl) (substr g 2 36)))
)
	


;;If the argument is a string of the form "leveln-...", where n is a non-negative integer, we will return the non-negative integer.  Else, return nil.
(defun extractLevelFromString (x 
	/
	regExp
	matches
	returnValue
	match
	submatches
	levelString
	)
	(setq returnValue nil)
	(setq regExp (vla-getinterfaceobject (vlax-get-acad-object) "VBScript.RegExp"))
	(vlax-put-property regExp "Pattern" "^level([\\d]+)-.*$")
	(vlax-put-property regExp "IgnoreCase" :vlax-true)
	(setq matches (vlax-invoke-method regExp "Execute" x))
	(if (> (vlax-get-property matches "Count") 0)
		(progn
			(setq match
				(vlax-get-property matches "Item" 0)
			)
			(setq submatches (vlax-get-property match "SubMatches"))
			;(princ (vlax-get-property submatches "Count"))
			(setq levelString (vlax-variant-value (vlax-get-property submatches "Item" 0)))
			(setq returnValue (atoi levelString))
		)
	)
	
	returnValue
)
	

;; the Documents collection's Add function, conveniently lets you create new drawing based on a template file, but it does
;; not let you create a new drawing based on an existing drawing.  This function fills that gap in functionality.	
(defun newDocumentBasedOnExisting (pathOfExistingDrawing 
	/
	result
	pathOfTemplate
	returnValue
	)

	;;(setq workingDrawing (vla-Add (vla-get-Documents (vlax-get-acad-object)) (cdr (assoc "pathOfSurveyFile" argument)))) ;;this creates a new document using the survey file as a template.
	;;the vla-Add Documents... only workse when the passed path is a template file dwt.  It does not work witha  dwg file.

	(setq pathOfTemplate (vl-filename-mktemp nil nil ".dwt"))
	(vl-file-delete pathOfTemplate) ;remove leftover template from a previous running, if it exists, so that the following copy will succeed, just in case.
	(setq result (vl-file-copy pathOfExistingDrawing pathOfTemplate))
	(if (not result) (progn(princ "file copy failed.")(princ "\n")))
	(setq returnValue (vla-Add (vla-get-Documents (vlax-get-acad-object)) pathOfTemplate))
	(setq result (vl-file-delete pathOfTemplate)) ;remove the template file, which was temporary and is no longer needed.	
	(if (not result)
		(progn
			(princ "deletion of temporary template file failed.\n")
		)
	)
	
	returnValue
)
;; Count Items  -  Lee Mac
;; Returns a list of dotted pairs detailing the number of
;; occurrences of each item in a supplied list.

; ; (defun LM:CountItems ( l / c x )
    ; ; (if (setq x (car l))
        ; ; (progn
            ; ; (setq c (length l)
                  ; ; l (vl-remove x (cdr l))
            ; ; )
            ; ; (cons (cons x (- c (length l))) (LM:CountItems l))
        ; ; )
    ; ; )
; ; )	
	
;; Count Items  -  Lee Mac
;; Returns a list of dotted pairs detailing the number of
;; occurrences of each item in a supplied list.


(defun executeScriptOnDrawing ( pathOfScriptFile pathOfDrawingFile /
		document
		documentList
	)
	;(setq document (LM:GetDocumentObject  pathOfDrawingFile)) ;;this does not produce a normal IAcadDocument object in the case that the document was not already opened, so I implemented my own version below, adapted from LM:GetDocumentObject .
	
	;this evaluates to the document object of the specified drawing iff. that drawing is open in autocad, else nil.
	(setq document 
		(cdr
			(assoc (strcase (findfile pathOfDrawingFile))
				;this evaluates to a dotted list whose elements are of the form (<documentName> . <documentObject>)
				(vlax-for doc (vla-get-documents (vlax-get-acad-object))
					(setq documentList (cons (cons (strcase (vla-get-fullname doc)) doc) documentList))
				)
			)
		)
	)
	
	(if (not document)
		(progn
			(setq document (vla-Open (vla-get-Documents (vlax-get-acad-object)) pathOfDrawingFile))
		)
	)


	
	;;(princ document)
	(vla-PostCommand document 
		(strcat "(load \"" (addslashes pathOfScriptFile) "\")\n" )
	)
	;;This postCommand still does not work quite right when the document was already open: in that case, PostCommand activates the document, and a manual reactivation of th original document
	;; is required before the script will continue.  
	;; curiously, this problem only seems to happen when the document we are posting to is the first document that executeScriptOnDrawing() has seen.
	;; at any rate, for now, I will be careful to only use (executeScriptOnDrawing ) on a document that is not already open.
)

;;I adapted the following addslashes function from Lee Mac's "escapeWildCards"
(defun addslashes ( str )
	(vl-list->string
		(apply 'append
			(mapcar
			   '(lambda ( c )
					(if (member c (vl-string->list "\\"))
						(list (car (vl-string->list "\\")) c)
						(list c)
					)
				)
				(vl-string->list str)
			)
		)
	)
)


(defun fullyPurgeDocument (document /
	)
	
	;; unfortunately, there is no straightforward api function to doa full recursive purge of a document.
	;; as a hack until I come up with a more thorough method, I will simply invoke the PurgeAll method
	;; several times in succession and hope that the nesting was not too deep.
	(repeat 30
		(vla-purgeAll document)
	)
)

; when you've got an anonymous block definition and want to know the name of the (non-anonymous) block on which it is based.
; this also works for non-anoymous block definitions, in which case it simply returns the name of the non-anonymous block.
(defun getEffectiveNameOfBlockDefinition (blockDefinition 
	/
		xDataGroupCodeForHandle
		handleOfNonAnonymousBlockDefinition
		nonAnonymousBlockDefinition
		parentDocument
		returnValue
	)
	(setq xDataGroupCodeForHandle 1005)
	(setq parentDocument (vla-get-Document blockDefinition))
	(setq handleOfNonAnonymousBlockDefinition (cdr (assoc xDataGroupCodeForHandle (gc:GetXdata blockDefinition "AcDbBlockRepBTag" ))))
	;;(princ "handleOfNonAnonymousBlockDefinition: ")(princ handleOfNonAnonymousBlockDefinition)(princ "\n")
	(setq nonAnonymousBlockDefinition 
		(if handleOfNonAnonymousBlockDefinition 
			(vla-HandleToObject parentDocument handleOfNonAnonymousBlockDefinition)
			blockDefinition
		)
	)
	;; at this point nonAnonymousBlockDefinition is either identically the blockDefinition argument that was passed in, or is the non-anonymous block definition on which blockDefinition is based.
	;; in either case, we are after the name of nonAnonymousBlockDefinition.
	(setq returnValue (vla-get-Name nonAnonymousBlockDefinition))
	returnValue
)

(defun LM:CountItems ( l / c l r x )
    (while l
        (setq x (car l)
              c (length l)
              l (vl-remove x (cdr l))
              r (cons (cons x (- c (length l))) r)
        )
    )
    (reverse r)
)
	
(defun processDocument 
	(
		argument
		/
		;;pathOfLogFile
		;;pathOfReportFile
		survey
		regionLayerNames
		regionCategories
		pathOfOutputDrawing
		result
		;;grandTotals
	)
	
	;; initialize grandTotals from the symbol passed as the "grandTotals" field of argument.
	;;(setq grandTotals (eval (cdr (assoc "grandTotals" argument))))
	;; grandTotals is an associative list whose keys are the regionCategory names and whose values are the total area.
	(setq numericPrecisionForAreaReport 1) ;; number of decimal places
	(setq pathOfLogFile (cdr (assoc "pathOfLogFile" argument)))
	; this ensures that the log file is empty. 
	;;(close (open pathOfLogFile "w")) 
	;; we will not empty the log file here, rather, we will leave this up to the calling makefile.

	
	(setq pathOfReportFile (strcat (cdr (assoc "outputDirectory" argument)) "\\" "report.txt"))
	; this ensures that the report file is empty. 
	(close (open pathOfReportFile "w"))

	(writeToLog "\n\n\n")
	(writeLineToLog (strcat "====" (rtos (getvar "CDATE") 2 6) "=== Now processing " (cdr (assoc "pathOfSurveyFile" argument)) ":" (cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument))))

	(setq workingDrawing (newDocumentBasedOnExisting (cdr (assoc "pathOfSurveyFile" argument))))
	
	(setq regionLayerNames (list))
	(vlax-for layer (vla-get-Layers workingDrawing) (appendTo 'regionLayerNames (vla-get-Name layer)))

	(setq regionLayerNames 
		(vl-remove-if
			; '(lambda (x) (member x '("0" "Defpoints" "gridLineLabels"  "gridLines"  "column" "exteriorWall")))
			'(lambda (x) (member x (cdr (assoc "nonRegionLayerNames" argument))))
			regionLayerNames
		)
	)
	;; we are not counting exteriorWall as a region because exterior wall is used only as an intermediate step to compute factory/warehouse (which is exterior wall minus everything else)

	;a region recipe is a list of elements of the form (rank regionLayerName),
	; where rank is either an integer or a list of integers, and regionLayerName is a string.
	; A region recipe is converted into a region by converting each regionLayerName into a region (by rankwise combining all the regions, polylines, and circles on the layer with name regionLayerName),
	; then the resulting list is passed as the argument to rankwiseCombineRegions.
	; a region recipe has exactly the same structure as an argument to (rankwiseCombineRegions),
	; except that, instead of regions, polylines, and circles, we have strings that are the names of layers.

	; a region category is an object that has the following properties:
	;	String name
	; 	RegionRecipe regionRecipe
	
	(setq regionLayerNames 
		(vl-remove-if
			; '(lambda (x) (member x '("0" "Defpoints" "gridLineLabels"  "gridLines"  "column" "exteriorWall")))
			'(lambda (x) (member x (list "office")))
			regionLayerNames
		)
	)
	;; I am going to construct 'office' explicitly as office layer - bathroom layer.
	
	(setq i 0)
	(setq regionCategories
		(mapcar 
			'(lambda (regionLayerName) 
				(list
					(cons "name" regionLayerName)
					(cons "regionRecipe" (list (list (list 0) regionLayerName)))
					(cons "color" (newColor (nth (rem (plusPlus 'i) (length (cdr (assoc "regionColorPallette" argument)))) (cdr (assoc "regionColorPallette" argument)))))
				)
			)
			regionLayerNames
		)
	)

	;; add office as a category
	(appendTo 'regionCategories
		(list
			(cons "name" "office")
			(cons "regionRecipe"
				(append
					(list
						(list
							(list 0) ; rank 0 is additive
							"office"
						)
					)
					(mapcar 
						'(lambda (x) 
							(list
								(list 1) ; rank 1 is subtractive
								x
							)
						)
						(list "bathroom")
					)
				)
			)
			(cons "color" (newColor (nth (rem (plusPlus 'i) (length (cdr (assoc "regionColorPallette" argument)))) (cdr (assoc "regionColorPallette" argument)))))
		)
	)

	;; add factory/warehouse as a category
	(appendTo 'regionCategories
		(list
			(cons "name" "factory/warehouse")
			(cons "regionRecipe"
				(append
					(list
						(list
							(list 0) ; rank 0 is additive
							"exteriorWall"
						)
					)
					(mapcar 
						'(lambda (x) 
							(list
								(list 1) ; rank 1 is subtractive
								x
							)
						)
						(vl-remove-if
							'(lambda (x)
								(= x "exteriorWall")
							)
							(append regionLayerNames (list "office"))
						)
						;; now that we are not including exteriorWall in regionLayerNames, the above remove-if doesn't do anything, but it doesn't hurt.
					)
				)
			)
			(cons "color" (newColor (nth (rem (plusPlus 'i) (length (cdr (assoc "regionColorPallette" argument)))) (cdr (assoc "regionColorPallette" argument)))))
		)
	)
	
	; ; footprint is the boolean sum of all other regionCategories.
	; (appendTo 'regionCategories
		; (list
			; (cons "name" "footprint")
			; (cons "regionRecipe"
				; (progn
					; (setq footprintRegionSpecifier (list ))
					; (setq i 0)
					; (foreach regionCategory regionCategories
						; (setq i (+ i 2))
						; (setq footprintRegionSpecifier
							; (append
								; footprintRegionSpecifier
								; (progn
									; (mapcar 
										; '(lambda (x / rank object newRank) 
											; (setq rank (car x))
											; (setq object (cadr x))
											; (setq newRank (append (list i) rank))
											; ;return: 
											; (list newRank object)
										; )
										; (cdr (assoc "regionRecipe" regionCategory))
									; )
								; )
								; ;;this rigamarrol -- prepending an increasing even integer to every rank -- is how we do a boolean sum of the regions created by each report item -- it has to do with the polygon ranking scheme in which ranks are lists rather than integers.  I really ought to hide all of this complexity behind some simple, nestable binary operations on rank/region lists.
							; )
						; )
					; )
					; footprintRegionSpecifier
				; )
			; )
		; )
	; )
	
	;;sort regionCategories by name alphabetically
	(setq regionCategories
		(vl-sort regionCategories
			'(lambda (a b)
				(< (cdr (assoc "name" a)) (cdr (assoc "name" b)))
			)
		)
	)

	;; prepare modelSpace by inserting the block definition specified in the argument 
	(if (= (cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument)) "*Model_Space")
		(progn
			; do nothing
		)
		(progn
			;;(setq blockDefinition (vla-item (vla-get-Blocks workingDrawing)  (cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument))))
			
			;delete everything currently in modelSpace
			(vlax-for entity (vla-get-ModelSpace workingDrawing) 
				(vla-Delete entity)
			)
			
			;insert a reference to blockDefinition
	`		(vla-InsertBlock (vla-get-ModelSpace workingDrawing) 
				(vlax-3D-point (list 0 0 0))     ;;insertion point
				(cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument))   ;;name of block to insert
				1 ;;Xscale 
				1 ;;Yscale
				1 ;;Zscale	
				0 ;; rotation angle.
			) 
		)
	)
	
	
	;; attempt to tag each entity with a level.
	;; An entity is said to have level n iff. (extractLevelFromString x) returns n.  (x is the name of the bolck definition that is the owner of the entity).  Otherwise, the level of the entity is nil.
	
	(setq levels (list))
	(vlax-for blockDefinition (vla-get-Blocks workingDrawing)
		(setq level (extractLevelFromString (getEffectiveNameOfBlockDefinition blockDefinition)))
		;; MY first stab at this was to use the "Name" property of the block.  But this caused problems with the anonymous blocks -- the problems being that the levels were not getting assigned within the anonymous blocks.
		;; The fix was to use the "EffectiveName" property instead.
		(if (and level (not (member level levels))) (appendTo 'levels level)) ; if we were able to extract a menaningful level and if that level does not already exsist in levels, then add it to levels.
		(setq i 0)
		(vlax-for entity blockDefinition 
			(progn
				(setLevel entity level)
				(if (/= (getLevel entity) level) (progn (princ "failed to set level on ")(princ (vla-get-ObjectName entity)) ))
				(plusPlus 'i)
			)
		)
		;;(princ "assigned level ")(princ level)(princ " to ")(princ i)(princ " entities in ")(princ (vla-get-Name blockDefinition))(princ "\n")
	)
	
	
	
	
	;;attempt to delete everything from the alignmentTargetDefinition, which will alignmentTarget lines from gettnig into the final drawing. (we wouldn't have to do this if the user had been careful to set all refereces to the alignmentTarget block to bconstruction)
	(setq alignmentTargetBlockDefinition 
		(vl-catch-all-apply
			'vla-Item (list (vla-get-Blocks workingDrawing) "alignmentTarget")
		)
	)
	(if (not (vl-catch-all-error-p alignmentTargetBlockDefinition))
		(progn
			(vlax-for entity alignmentTargetBlockDefinition
				(vl-catch-all-apply 'vla-Delete (list entity))
			)
		)
	)
	
	(recursivelyExplodeAllBlockReferencesWithin (vla-get-ModelSpace workingDrawing) )
	; At this point, modelSpace contains no block references.

	(setq pathOfWorkingDrawing 
		(strcat 
			(cdr (assoc "outputDirectory" argument)) "\\" 
			(if (= (cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument)) "*Model_Space") 
				(Strcat
					(vl-filename-base (cdr (assoc "pathOfSurveyFile" argument)))  
					"-processed"
				)
				(cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument))
			)
			"-working"
			".dwg"
		)
	)
	(writeToLog "\t")(writeToLog "pathOfWorkingDrawing: ")(writeToLog pathOfWorkingDrawing)(writeToLog "\n")
	(vl-file-delete pathOfWorkingDrawing) ;remove leftover working drawing from a previous running, if it exists, so that the following copy will succeed.
	(vla-SaveAs workingDrawing pathOfWorkingDrawing)
	(vla-Close workingDrawing)
	(vlax-release-object workingDrawing)
	;;attempt to delete the *.bak file that might have been created alongside the workingDrawing file.
	(vl-file-delete 		
		(strcat 
			(substr pathOfWorkingDrawing  1 (- (strlen pathOfWorkingDrawing) (strlen (vl-filename-extension pathOfWorkingDrawing))))
			".bak"
		)
	)

	;;=====  create a drawing, whose name will be suffixed with "-footprint", whose model space contains a region 
	;; (and hatch, perhaps) of the building footprint, which is the union of all regions in all levels of the building
	;; we will accumulate the contributing regions in footprintDrawing and then union them all once we have processed all levels.
	(progn
		(setq footprintDrawing (newDocumentBasedOnExisting pathOfWorkingDrawing))
			
		; clear footprintDrawing's modelSpace

		(vlax-for entity (vla-get-ModelSpace footprintDrawing)
			(vla-Delete entity)
		)
		
		;;(fullyPurgeDocument footprintDrawing)
	)
	(setq footprintContributors (list)) ;;footprintContributors will be a list of regions in footprintDrawing (which we will copy into footprintDrawing as we go), which we will union together after processing all levels to create the footprint region.
	


	(foreach level levels
		(setq outputDrawing (newDocumentBasedOnExisting pathOfWorkingDrawing))
			
		(setq layerRenameDirectives (list)) 
		;; layerRenameDirectives is a list of dotted pairs of the form (layer . newName)
		;; this will be used to keep track of new layers that we create (which initially will be given a random name so as not to conflict with any existing layers), then later (once all original layers have been deleted), we will apply the newName to each layer.
		(setq totalFloorAreaOnThisLevel 0.0)
		(setq objectsToKeep (list)) ; we keep track of the new things that we create, and anything else we want to not delete, so we can avoid deleting them when we purge leftovers.
		
		(setq otherBlockDefinitionsToInclude
			(mapcar
				'(lambda (x) 
					(vla-item (vla-get-Blocks outputDrawing) x)
				)
				(cdr (assoc "namesOfOtherBlockDefinitionsToInclude" argument))
			)
		)
		(setq objectsToKeep (append objectsToKeep otherBlockDefinitionsToInclude))
		
		(setq reportDrawing (newDocumentBasedOnExisting (cdr (assoc "pathOfReportTemplate" argument))))
		(vlax-for layout (vla-get-Layouts reportDrawing)
			(vl-catch-all-apply 'vla-Delete (list layout))
		)
		(setq reportTable 
			(vla-AddTable (vla-get-ModelSpace reportDrawing)
				(vlax-3D-point (list 0 0 0)) ;;insertion point
				1 ;;numRows
				3 ;;numColumns
				0.001 ;; rowHeight
				1.0 ;; column width
			)
		)


		
		(vla-setText reportTable 
			0 ;;rowIndex
			0 ;;columnIndex
			(strcat 
				"Floor Area Analysis" "\n"
				(cdr (assoc "nameOfBuilding" argument)) ", " "Level " (itoa level)				
			)
		)
		;; we expect that the first row of the table is merged into a single cell, and serves as a title
		;; for the table.
		;;construct a header row
		(vla-InsertRows reportTable
			(vla-get-rows reportTable) ;; row index at which to insert the rows
			0.001 ;; row height
			1 ;;number of rows to insert
		)
		(vla-setText reportTable 
			1 ;;rowIndex
			0 ;;columnIndex
			"Color"
		)
		(vla-setText reportTable 
			1 ;;rowIndex
			1 ;;columnIndex
			"Category"
		)
		(vla-setText reportTable 
			1 ;;rowIndex
			2 ;;columnIndex
			(strcat "Area" "\n" "(square feet)")
		)
		
		
		
		
		
		
		(setq allBoundariesLayer (vla-Add (vla-get-Layers outputDrawing) (GUID)))
		(appendTo 'objectsToKeep allBoundariesLayer)
		(appendTo 'layerRenameDirectives (cons allBoundariesLayer  "boundary"))
		(vla-put-LayerOn allBoundariesLayer :vlax-true)
		
		(foreach regionCategory regionCategories
			(writeLineToLog (strcat "now processing regionCategory with name: " (cdr (assoc "name" regionCategory))))
			(writeToLog "\t")(writeToLog "regionRecipe: ")(writeToLog (cdr (assoc "regionRecipe" regionCategory)))(writeToLog "\n")
			(writeLineTo pathOfReportFile (cdr (assoc "name" regionCategory)))
			;(setq regionRecipe (cdr (assoc "regionRecipe" regionCategory)))
			
			; assemble a list of regionSources, each of which is either a 
			; a region or a closed polyline.
			;  these regionSources will be rankwise-combined to produce the final region in the output file.
			(setq newlyCreatedRegions (list)) ; we keep track of these as we go through the elements of the region recipe so that we ignore newly created regions. (if you do not keep track, there is the possibility that a region created while processing the first element of regionRecipe gets used as a region source when processing the second regionRecipe.
			(setq rankedRegions 
				(mapcar
					'(lambda (x / regionSources region regionLayerName returnValue) 
						(setq regionLayerName (cadr x))
						(setq regionSources (list))
						(vlax-for entity (vla-get-ModelSpace outputDrawing) 
							(if 
								(and
									(or
										(= "AcDbPolyline" (vla-get-ObjectName entity))
										(= "AcDbCircle" (vla-get-ObjectName entity))
										(= "AcDbRegion" (vla-get-ObjectName entity))
									)
									(= regionLayerName (vla-get-Layer entity))
									(= (getLevel entity) level)
									(not (member (vla-get-Handle entity) (mapcar 'vla-get-Handle newlyCreatedRegions)))
								)
								(progn
									; (setq regionSources
										; (append
											; regionSources
											; (list entity)
										; )
									; )
									(appendTo 'regionSources entity)
								)
							)
						)
						;====		
						(writeLineToLog (strcat "\t" "number of regionSources on layer " regionLayerName ": " (itoa (length regionSources))))
						
						(if (> (length regionSources) 0)
							(progn
								(writeToLog (strcat "\t" "regionSources on " regionLayerName ": "))(writeToLog regionSources)(writeToLog "\n")
								(setq region (rankwiseCombineRegions regionSources))
								(writeToLog (strcat "\t" "region: "))(writeToLog region)(writeToLog "\n")
								(if region
									(progn
										(appendTo 'newlyCreatedRegions region)
										(setq returnValue (list (car x) region))
									)
									(progn
										(setq returnValue nil)
									)
								)
							)
							(progn
								(setq returnValue nil)
							)
						)
						; finally, return the result:
						
						returnValue
					)
					(cdr (assoc "regionRecipe" regionCategory))
				)
			)
			
			(writeToLog "\t")(writeToLog "before removing nils, rankedRegions: ")(writeToLog rankedRegions)(writeToLog "\n")
			; remove any nil elements from rankedRegions (this would have happened if one of the elements of regionRecipe could not be converted into a region.
			(setq rankedRegions 
				(vl-remove-if 'not
					rankedRegions
				)
			)
			(writeToLog "\t")(writeToLog "rankedRegions: ")(writeToLog rankedRegions)(writeToLog "\n")
			
			;; attempt to create region and hatch.
			(setq region nil)
			(setq hatch nil)
			
			(setq region (rankwiseCombineRegions rankedRegions))
			
			(if region 
				(progn 
					(writeToLog "now attempting to add a hatch to region (")(writeToLog (vla-get-Handle region))(writeToLog ":")(writeToLog (vla-get-ObjectName region))(writeToLog ") : ")
					
					;;I have put everything in the following (if nil...) block into a function called makeHatchFromRegion defined in regionFunctions.lsp
					(if nil 
						(progn
							;;(if (= "factory/warehouse" (cdr (assoc "name" regionCategory))) (progn (quit ))) ;;debugging
							(setq hatch 
								(vla-AddHatch (vla-get-ModelSpace outputDrawing)
									1 			;; patternType
									"SOLID" 	;; patternName
									:vlax-false	;; associativity
								)
							)
							
							
							(setq e 
								(vl-catch-all-apply 
									'vla-AppendOuterLoop (list hatch (gc:ObjectListToVariant (list region)))
									;; this will throw an excpetion if region happens to be disjoint. (curiously the ui HATCH function allows you to select a disjoint region and works without error)
									;; if that happens, the fix is to explode region, which produces multiple
									;; non-disjoint regions.
									;; We cannot just explode region in all cases, because exploding a non-disjoint region 
									;; produces the boundary lines, arcs, etc. (Crazy)
								)
							)
							(if (vl-catch-all-error-p  e)
								(progn
									(writeToLog " failed with exception: ")(writeToLog (vl-catch-all-error-message e))(writeToLog "\n")
									;;(if (= "factory/warehouse" (cdr (assoc "name" regionCategory))) (progn (quit ))) ;;debugging
									;; if we get here, it means that vla-AppendOuterLoop threw an exception.
									;; so, we need to explode the region and try again.			
									(setq explosionProducts (gc:VariantToLispData (vla-Explode region)))
									(foreach explosionProduct explosionProducts
										
										(setq setasideExplosionProduct explosionProduct) ;;this is for debugging
										(writeToLog "now appending explosionProduct (")(writeToLog (vla-get-Handle explosionProduct))(writeToLog ":")(writeToLog (vla-get-ObjectName explosionProduct))(writeToLog ") as outer loop to hatch: ")
										(setq e 
											(vl-catch-all-apply 
												'vla-AppendOuterLoop (list hatch (gc:ObjectListToVariant (list explosionProduct)))
											)
										)
										(if (vl-catch-all-error-p  e)
											(progn
												(writeToLog "failed with exception: ")(writeToLog (vl-catch-all-error-message e))(writeToLog "\n")
											)
											(progn 
												(writeToLog " succeeded.")(writeToLog "\n")
											)
										)
										
										(vla-Delete explosionProduct)
									)
									;; by making a copy of the region, then exploding the copy, and after creating the hatch, deleting the explosion poroducts, we are left with one hatch and one region (the original region) as desired.  If we did not create a copy of the region, we would be left with 5 regions, which breaks the pattern (Autocad is nice enough to deal intelligently with, and have a concept of, a disjoint region -- we might as well use this capability.)			)
									;; actually, we do not need to make a copy of the region before exploding it because the vlax explode fuinction (unlike the UI EXPLODE function) preserves the original object.
								)
								(progn 
									(writeToLog " succeeded.")(writeToLog "\n")
								)
							)
						)
					)
					(setq hatch (makeHatchFromRegion region))
					;;(writeToLog "the hatch is ")(writeToLog hatch)(writeToLog "\n")
					;;(vlax-dump-object hatch)
					;;(vla-put-PatternType hatch acHatchPatternTypePreDefined)
					;;(vla-put-PatternName hatch "SOLID")
					;; oops - patternName and PatternType are read-only properties.
					(vla-SetPattern hatch acHatchPatternTypePreDefined "SOLID")
					
					
					;;copy region into footprintDrawing and add region to footprintContributors
					(setq result 
						(vla-CopyObjects 
							(vla-get-Document region) 			        ; the database whose "CopyObjects" method we are calling (this is the database from which we are copying things)
							(gc:ObjectListToVariant (list region))		; the list of objects to be copied
							(vla-get-ModelSpace footprintDrawing) 	; the owner to whom thses objects will be copied
						)
					)
					;;if everything worked as expected, result will be a variant array of the new entities, which should consist of exactly one entity (the newly created region).
					(appendTo 'footprintContributors (nth 0 (gc:VariantToLispData result)))
				)
			)

			
			(setq outputLayer (vla-Add (vla-get-Layers outputDrawing) (GUID)))
			
			(appendTo 'objectsToKeep outputLayer)
			(appendTo 'layerRenameDirectives (cons outputLayer  (cdr (assoc "name" regionCategory))))
			(vla-put-ActiveLayer outputDrawing outputLayer)
			(vla-put-LayerOn outputLayer :vlax-true)
			
			(if region 
				(progn	
					(vla-put-layer region (vla-get-Name outputLayer))
					;;(appendTo 'objectsToKeep region)
					;; I have commented out the above line in order not to have the region in the final output drawing, because
					;; a region object does not plot predictably,- at least it does not respond to the lineweight setting, but rather always plots with something like zero lineweight (i.e. thinnest possible lineweight).
					;; Instead of the region, I will include the boundary of the region:
					(setq boundaryLines (fullyExplodeRegion region))
					(foreach boundaryLine boundaryLines
						(vla-put-layer boundaryLine (vla-get-Name allBoundariesLayer))
						(appendTo 'objectsToKeep boundaryLine)
					)
				)
			)
			(if hatch 
				(progn	
					(vla-put-layer hatch (vla-get-Name outputLayer))
					(appendTo 'objectsToKeep hatch)
					;;(vla-put-BackgroundColor hatch (cdr (assoc "color" regionCategory)))
					(vla-put-TrueColor hatch (cdr (assoc "color" regionCategory)))
				)
			)
			
			
			

			
			(if region
				(progn
					(vla-InsertRows reportTable
						(vla-get-rows reportTable) ;; row index at which to insert the rows
						0.2 ;; row height
						1 ;;number of rows to insert
					)
					(vla-SetCellBackgroundColor reportTable 
						(- (vla-get-rows reportTable) 1) ;;rowIndex
						0 ;;columnIndex
						(cdr (assoc "color" regionCategory))
					)
					(vla-setText reportTable 
						(- (vla-get-rows reportTable) 1) ;;rowIndex
						1 ;;columnIndex
						(vl-string-translate "_" " " (cdr (assoc "name" regionCategory))) ;;replace underscore with space
					)
					(vla-setText reportTable 
						(- (vla-get-rows reportTable) 1) ;;rowIndex
						2 ;;columnIndex
						(if region (strcat (rtos (/ (vla-get-Area region) (expt foot 2)) 2 numericPrecisionForAreaReport) ) "zero")
					)
					(setq totalFloorAreaOnThisLevel (+ totalFloorAreaOnThisLevel (vla-get-Area region)))
					;;update grandTotals
					(setItem (getItem argument "grandTotals")
						(cdr (assoc "name" regionCategory))
						(+ 
							(if (getItem (getItem argument "grandTotals") (cdr (assoc "name" regionCategory))) (getItem (getItem argument "grandTotals") (cdr (assoc "name" regionCategory))) 0) ;;we take 0 to be the accumulated area in this category if the entry does not already exist in grandTotals.
							(vla-get-Area region)
						)
					)
				)
			)
			
			
			(writeLineTo pathOfReportFile 
				(strcat 
					"\t" "area: " (if region (strcat (rtos (/ (vla-get-Area region) (expt foot 2)) 2 1) " square feet") "(empty)") 
				)
			)
			
			(writeLineTo pathOfReportFile "")
			(writeLineToLog "")
			
		)

		
		

		;;clean up leftover layers and block, etc.
		
		;;add a TOTAL line to the report tabl;
		(vla-InsertRows reportTable
			(vla-get-rows reportTable) ;; row index at which to insert the rows
			0.2 ;; row height
			1 ;;number of rows to insert
		)
		;; thicken the horizontal rule above the total row
		(foreach columnIndex (range (vla-get-columns reportTable))
			(vla-SetCellGridLineWeight  reportTable
				(- (vla-get-rows reportTable) 1)  ;; last row ;;rowIndex
				columnIndex ;;columnIndex
				acTopMask ;; edges
				acLnWt070 ;; lineweight
			)
		)
		;;there shall be no background color in the "Color" field of the total row.
		(vla-SetCellBackgroundColorNone reportTable 
			(- (vla-get-rows reportTable) 1) ;;rowIndex
			0 ;;columnIndex
			:vlax-true
		)
		(vla-setText reportTable 
			(- (vla-get-rows reportTable) 1) ;;rowIndex
			1 ;;columnIndex
			"TOTAL"
		)
		(vla-setText reportTable 
			(- (vla-get-rows reportTable) 1) ;;rowIndex
			2 ;;columnIndex
			(strcat (rtos (/ totalFloorAreaOnThisLevel (expt foot 2)) 2 numericPrecisionForAreaReport) )
		)

		
		;;set all table text to uppercase
		(foreach rowIndex (range (vla-get-rows reportTable))
			(foreach columnIndex (range (vla-get-columns reportTable))
				(vla-setText reportTable 
					rowIndex ;;rowIndex
					columnIndex ;;columnIndex
					(strcase 
						(vla-getText reportTable 
							rowIndex ;;rowIndex
							columnIndex ;;columnIndex
						)	
					)
				)	
			)
		)
		
		(vla-SetAlignment reportTable 
			acDataRow ;;rowTypes
			acBottomLeft ;;cellAlignment
		)
		(vla-SetAlignment reportTable
			acHeaderRow ;;rowTypes
			acBottomLeft ;;cellAlignment
		)
		(vla-SetAlignment reportTable
			acTitleRow ;;rowTypes
			acBottomLeft ;;cellAlignment
		)
		(vla-SetTextHeight reportTable
			acDataRow ;;rowTypes
			(/ 12.0 72) ;;textHeight
		)
		(vla-SetTextHeight reportTable
			acHeaderRow ;;rowTypes
			(/ 12.0 72) ;;textHeight
		)
		(vla-SetTextHeight reportTable
			acTitleRow ;;rowTypes
			(/ 12.0 72) ;;textHeight
		)
		
		(vla-SetColumnWidth reportTable
			0 ;;columnIndex
			0.95 ;;width
		)
		(vla-SetColumnWidth reportTable
			1 ;;columnIndex
			3.15 ;;width
		)
		(vla-SetColumnWidth reportTable
			2 ;;columnIndex
			2 ;;width
		)
		
		(foreach rowIndex (range (vla-get-rows reportTable))
			(vla-setRowHeight reportTable rowIndex 0.001) ;;we set the row height to almost zero, so that the natural height of the text determines the row height.
		)
		;;right-align the column of numbers
		(foreach rowIndex (cdr (range (vla-get-rows reportTable))) ;;for all rows after the first row
			(vla-setCellAlignment reportTable 
				rowIndex ;;rowIndex
				2 ;;columnindex
				acTopRight
			)
		)

		
		(fullyPurgeDocument reportDrawing)
		
		(foreach blockDefinition otherBlockDefinitionsToInclude
			;insert a reference to blockDefinition
			(setq blockReference 
				(vla-InsertBlock (vla-get-ModelSpace outputDrawing) 
					(vlax-3D-point (list 0 0 0))     ;;insertion point
					(vla-get-Name blockDefinition)   ;;name of block to insert
					1 ;;Xscale 
					1 ;;Yscale
					1 ;;Zscale	
					0 ;; rotation angle.
				) 
			)
			(if blockReference 
				(progn	
					(appendTo 'objectsToKeep blockReference)
					(vla-put-Layer blockReference "0")
				)
			)
		)
		
		; In model space, delete all entities except the newly created entities

		(vlax-for entity (vla-get-ModelSpace outputDrawing)
			(if (member (vla-get-Handle entity) (mapcar 'vla-get-Handle objectsToKeep)) ;;if this entity is one of our newly created entities...
				(progn
					; do nothing
				)
				(progn ;;else,
					(vla-Delete entity)
				)
			)
		)

		
		(if nil ;; I am purging instead.
			(progn
				; delete all block definitions except modelSpace
				(vlax-for blockDefinition (vla-get-Blocks outputDrawing) 
					(if 
						(or 
							(objectsHaveSameHandle blockDefinition (vla-get-ModelSpace outputDrawing))
							(=  (vla-get-Name blockDefinition) "*Paper_Space")
						)
						(progn
							; do nothing
						)
						(progn
							;;(writeToLog "now attempting to delete block definition \"")(writeToLog (vla-get-Name blockDefinition))(writeToLog "\".")(writeToLog "\n")
							(vla-Delete blockDefinition)
						)
					)
				)


				; delete all layers except the newly created layers and layer zero
				(vlax-for layer (vla-get-Layers outputDrawing)

					(if 
						(member (vla-get-Handle layer) 
							(mapcar 'vla-get-Handle 
								(append objectsToKeep (list (vla-Item (vla-get-Layers outputDrawing) "0")))
							)
						)
						(progn
							;do nothing
						)
						(progn
							(vla-delete layer)
						)
					)
					
				)
			)
		)

		
		
		
		;;rename the newly created layers
		(foreach layerRenameDirective layerRenameDirectives
			(setq layer (car layerRenameDirective))
			(setq newName (sanitizeName (cdr layerRenameDirective)))
			
			;; if an existing layer has the name newName, then we need to rename the existing layer
			(if 
				(vl-catch-all-error-p 
					(vl-catch-all-apply 'vla-Item (list (vla-get-Layers outputDrawing) newName))
				)
				(progn
					; in this case, the newName is not already in use, so we do not need to do anything
				)
				(progn
					; in this case, newName is already being used, so we need to rename the existing layer
					(setq existingLayer  (vla-Item (vla-get-Layers outputDrawing) newName))
					(vla-put-Name existingLayer (strcat (vla-get-Name existingLayer) "--" (GUID)))
					(setq existingLayer nil)
				)
			)
			;; at this point, we are guaranteed that no layer exists with the name newName.
			
			(vl-catch-all-apply 'vla-put-Name (list layer newName))
		)
		

		
		(fullyPurgeDocument outputDrawing)
		
		(setq pathOfOutputDrawing 
			(strcat 
				(cdr (assoc "outputDirectory" argument)) "\\" 
				(if (= (cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument)) "*Model_Space") 
					(Strcat
						(vl-filename-base (cdr (assoc "pathOfSurveyFile" argument)))  
						"-processed"
					)
					(cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument))
				)
				"-level" (itoa level)
				".dwg"
			)
		)
		(vl-file-delete pathOfOutputDrawing) ;remove leftover outputDrawing from a previous running, if it exists, so that the following saveAs will succeed.
		(vla-SaveAs outputDrawing pathOfOutputDrawing)
		(vla-Close outputDrawing)
		(vlax-release-object outputDrawing)
		;;attempt to delete the *.bak file that might have been created alongside the outputDrawing file.
		(vl-file-delete 		
			(strcat 
				(substr pathOfOutputDrawing  1 (- (strlen pathOfOutputDrawing) (strlen (vl-filename-extension pathOfOutputDrawing))))
				".bak"
			)
		)
		
		
		(setq pathOfReportDrawing
			(strcat 
				(cdr (assoc "outputDirectory" argument)) "\\" 
				(vl-filename-base pathOfOutputDrawing)
				"-report"
				".dwg"
			)
		)
		
		(vl-file-delete pathOfReportDrawing) ;remove leftover reportDrawing from a previous running, if it exists, so that the following saveAs will succeed.
		(vla-SaveAs reportDrawing pathOfReportDrawing)
		(vla-Close reportDrawing)
		(vlax-release-object reportDrawing)
		;;attempt to delete the *.bak file that might have been created alongside the reportDrawing file.
		
		(vl-file-delete 		
			(strcat 
				(substr pathOfReportDrawing  1 (- (strlen pathOfReportDrawing) (strlen (vl-filename-extension pathOfReportDrawing))))
				".bak"
			)
		)
		
	)
	
	;;=====  create a drawing, whose name will be suffixed with "-background", whose model space contains nothing except references to the blocks specified in namesOfOtherBlockDefinitionsToInclude
	(progn
		(setq backgroundDrawing (newDocumentBasedOnExisting pathOfWorkingDrawing))
		(setq objectsToKeep (list)) ; we keep track of the new things that we create, and anything else we want to not delete, so we can avoid deleting them when we purge leftovers.
		
		(setq otherBlockDefinitionsToInclude
			(mapcar
				'(lambda (x) 
					(vla-item (vla-get-Blocks backgroundDrawing) x)
				)
				(cdr (assoc "namesOfOtherBlockDefinitionsToInclude" argument))
			)
		)
		(setq objectsToKeep (append objectsToKeep otherBlockDefinitionsToInclude))
				
		(foreach blockDefinition otherBlockDefinitionsToInclude
			;insert a reference to blockDefinition
			(setq blockReference 
				(vla-InsertBlock (vla-get-ModelSpace backgroundDrawing) 
					(vlax-3D-point (list 0 0 0))     ;;insertion point
					(vla-get-Name blockDefinition)   ;;name of block to insert
					1 ;;Xscale 
					1 ;;Yscale
					1 ;;Zscale	
					0 ;; rotation angle.
				) 
			)
			(if blockReference 
				(progn	
					(appendTo 'objectsToKeep blockReference)
					(vla-put-Layer blockReference "0")
				)
			)
		)
		
		; In model space, delete all entities except the newly created entities

		(vlax-for entity (vla-get-ModelSpace backgroundDrawing)
			(if (member (vla-get-Handle entity) (mapcar 'vla-get-Handle objectsToKeep)) ;;if this entity is one of our newly created entities...
				(progn
					; do nothing
				)
				(progn ;;else,
					(vla-Delete entity)
				)
			)
		)
		
		(fullyPurgeDocument backgroundDrawing)

		(setq pathOfBackgroundDrawing 
			(strcat 
				(cdr (assoc "outputDirectory" argument)) "\\" 
				(if (= (cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument)) "*Model_Space") 
					(Strcat
						(vl-filename-base (cdr (assoc "pathOfSurveyFile" argument)))  
						"-processed"
					)
					(cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument))
				)
				"-background"
				".dwg"
			)
		)
		(vl-file-delete pathOfBackgroundDrawing) ;remove leftover backgroundDrawing from a previous running, if it exists, so that the following saveAs will succeed.
		(vla-SaveAs backgroundDrawing pathOfBackgroundDrawing)
		(vla-Close backgroundDrawing)
		(vlax-release-object backgroundDrawing)
		;;attempt to delete the *.bak file that might have been created alongside the backgroundDrawing file.
		(vl-file-delete 		
			(strcat 
				(substr pathOfBackgroundDrawing  1 (- (strlen pathOfBackgroundDrawing) (strlen (vl-filename-extension pathOfBackgroundDrawing))))
				".bak"
			)
		)
	)
	
	;== finalize footprintDrawing
	(progn
		(writeLineToLog "finalizing footprint drawing.")
		(setq footprintEntitiesToKeep (list ))
		
		(writeToLog "merging ")(writeToLog (length footprintContributors))(writeToLog " contributor regions into the footprintRegion.")(writeToLog "\n")
		; ; ; ;; at this point, we have footprintContributors, a list of regions in footprintDrawing, which are to be unioned together to create the footprintRegion.
		(setq footprintRegion (vl-catch-all-apply 'rankwiseCombineRegions (list footprintContributors)))
		(if (vl-catch-all-error-p footprintRegion)
			(progn
				(writeLineToLog "failed to create footprintRegion.")
			)
			(progn
				(writeToLog "footprint creation succeeded.  ")(writeToLog "footprintRegion: ")(writeToLog footprintRegion)(writeToLog "\n")
			)
		)
		(appendTo 'footprintEntitiesToKeep footprintRegion)
		(writeLineToLog "adding footprintBoundaryLines.")
		(setq footprintBoundaryLines (fullyExplodeRegion footprintRegion))
		(foreach boundaryLine footprintBoundaryLines
			(appendTo 'footprintEntitiesToKeep boundaryLine)
		)
		
		
		(writeLineToLog "adding footprintHatch.")
		(setq footprintHatch (makeHatchFromRegion footprintRegion))
		(appendTo 'footprintEntitiesToKeep footprintHatch)
		(vla-SetPattern footprintHatch acHatchPatternTypePreDefined "SOLID")
		(writeLineToLog "adding layers.")
		;; create a separate layer for each of 'region' 'boundary' and 'hatch'
		(vl-catch-all-apply 'vla-add (list (vla-get-layers footprintDrawing) "region"))
		(vl-catch-all-apply 'vla-add (list (vla-get-layers footprintDrawing) "boundary"))
		(vl-catch-all-apply 'vla-add (list (vla-get-layers footprintDrawing) "hatch"))
		(writeLineToLog "assigning layers.")
		(vl-catch-all-apply 'vla-put-layer (list footprintRegion "region"))
		(vl-catch-all-apply 'vla-put-layer (list footprintHatch "hatch"))
		(foreach footprintBoundaryLine footprintBoundaryLines
			(vl-catch-all-apply 'vla-put-layer (list footprintBoundaryLine "boundary"))
		)

		(writeLineToLog "deleting any leftover entities that we don't want to keep.")	
		(vlax-for entity (vla-get-ModelSpace footprintDrawing)
			(if (member (vla-get-Handle entity) (mapcar 'vla-get-Handle footprintEntitiesToKeep)) ;;if this entity is one of footprintEntitiesToKeep
				(progn
					; do nothing
				)
				(progn ;;else,
					(vl-catch-all-apply 'vla-Delete (list entity))
				)
			)
		)
	
		(fullyPurgeDocument footprintDrawing)
		(setq pathOfFootprintDrawing 
			(strcat 
				(cdr (assoc "outputDirectory" argument)) "\\" 
				(if (= (cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument)) "*Model_Space") 
					(Strcat
						(vl-filename-base (cdr (assoc "pathOfSurveyFile" argument)))  
						"-processed"
					)
					(cdr (assoc "nameOfBlockDefinitionThatContainsRegionShapes" argument))
				)
				"-footprint"
				".dwg"
			)
		)
		(vl-file-delete pathOfFootprintDrawing) ;remove leftover footprintDrawing from a previous running, if it exists, so that the following saveAs will succeed.
		(vla-SaveAs footprintDrawing pathOfFootprintDrawing)
		(vla-Close footprintDrawing)
		(vlax-release-object footprintDrawing)
		;;attempt to delete the *.bak file that might have been created alongside the footprintDrawing file.
		(vl-file-delete 		
			(strcat 
				(substr pathOfFootprintDrawing  1 (- (strlen pathOfFootprintDrawing) (strlen (vl-filename-extension pathOfFootprintDrawing))))
				".bak"
			)
		)
	)
	
	;;create grandTotalsReportDrawing
	;; it is a bit inelegant to make the grandTotalsReportDrawing at the end of processing each building, when we really only need to do it at the end of the whole running
	;; but for now, it will get the job done. (the grandTotals report should be generated in the processDocuments() function.
	(progn
		(setq numericPrecisionForAreaReport 0) ;; number of decimal places
		(setq grandTotalsReportDrawing (newDocumentBasedOnExisting (cdr (assoc "pathOfReportTemplate" argument))))
		; clear grandTotalsReportDrawing's modelSpace
		(vlax-for entity (vla-get-ModelSpace grandTotalsReportDrawing)
			(vla-Delete entity)
		)
		
		(vlax-for layout (vla-get-Layouts grandTotalsReportDrawing)
			(vl-catch-all-apply 'vla-Delete (list layout))
		)
		(setq reportTable 
			(vla-AddTable (vla-get-ModelSpace grandTotalsReportDrawing)
				(vlax-3D-point (list 0 0 0)) ;;insertion point
				1 ;;numRows
				2 ;;numColumns
				0.001 ;; rowHeight
				1.0 ;; column width
			)
		)
		
		(vla-setText reportTable 
			0 ;;rowIndex
			0 ;;columnIndex
			(strcat 
				"Floor Area Analysis" "\n"
				"all buildings, all levels"				
			)
		)
		
		;; we expect that the first row of the table is merged into a single cell, and serves as a title
		;; for the table.
		;;construct a header row
		
		(vla-InsertRows reportTable
			(vla-get-rows reportTable) ;; row index at which to insert the rows
			0.001 ;; row height
			1 ;;number of rows to insert
		)
		(vla-setText reportTable 
			1 ;;rowIndex
			0 ;;columnIndex
			"Category"
		)
		(vla-setText reportTable 
			1 ;;rowIndex
			1 ;;columnIndex
			(strcat "Area" "\n" "(square feet)")
		)
		
		(setq grandTotalArea 0)
		(foreach regionCategory  regionCategories
			(setq areaOfThisRegionCategory 
				(getItem (getItem argument "grandTotals") (cdr (assoc "name" regionCategory)))
			)
			
			(if areaOfThisRegionCategory
				(progn
					(vla-InsertRows reportTable
						(vla-get-rows reportTable) ;; row index at which to insert the rows
						0.2 ;; row height
						1 ;;number of rows to insert
					)
					; (vla-SetCellBackgroundColor reportTable 
						; (- (vla-get-rows reportTable) 1) ;;rowIndex
						; 0 ;;columnIndex
						; (cdr (assoc "color" regionCategory))
					; )
					(vla-setText reportTable 
						(- (vla-get-rows reportTable) 1) ;;rowIndex
						0 ;;columnIndex
						(vl-string-translate "_" " " (cdr (assoc "name" regionCategory))) ;;replace underscore with space
					)
					(vla-setText reportTable 
						(- (vla-get-rows reportTable) 1) ;;rowIndex
						1 ;;columnIndex
						(strcat (rtos (/ areaOfThisRegionCategory (expt foot 2)) 2 numericPrecisionForAreaReport) )
					)
					
					(setq grandTotalArea (+ grandTotalArea areaOfThisRegionCategory))
				)
			)
		)
		
		
		
		
		
		
		;;add a TOTAL line to the report tabl;
		(vla-InsertRows reportTable
			(vla-get-rows reportTable) ;; row index at which to insert the rows
			0.2 ;; row height
			1 ;;number of rows to insert
		)
		;; thicken the horizontal rule above the total row
		(foreach columnIndex (range (vla-get-columns reportTable))
			(vla-SetCellGridLineWeight  reportTable
				(- (vla-get-rows reportTable) 1)  ;; last row ;;rowIndex
				columnIndex ;;columnIndex
				acTopMask ;; edges
				acLnWt070 ;; lineweight
			)
		)
		
		; ;;there shall be no background color in the "Color" field of the total row.
		; (vla-SetCellBackgroundColorNone reportTable 
			; (- (vla-get-rows reportTable) 1) ;;rowIndex
			; 0 ;;columnIndex
			; :vlax-true
		; )
		
		(vla-setText reportTable 
			(- (vla-get-rows reportTable) 1) ;;rowIndex
			0 ;;columnIndex
			"TOTAL"
		)
		
		(vla-setText reportTable 
			(- (vla-get-rows reportTable) 1) ;;rowIndex
			1 ;;columnIndex
			(strcat (rtos (/ grandTotalArea (expt foot 2)) 2 numericPrecisionForAreaReport) )
		)

		
		;;set all table text to uppercase
		(foreach rowIndex (range (vla-get-rows reportTable))
			(foreach columnIndex (range (vla-get-columns reportTable))
				(vla-setText reportTable 
					rowIndex ;;rowIndex
					columnIndex ;;columnIndex
					(strcase 
						(vla-getText reportTable 
							rowIndex ;;rowIndex
							columnIndex ;;columnIndex
						)	
					)
				)	
			)
		)
		
		(vla-SetAlignment reportTable 
			acDataRow ;;rowTypes
			acBottomLeft ;;cellAlignment
		)
		
		(vla-SetAlignment reportTable
			acHeaderRow ;;rowTypes
			acBottomLeft ;;cellAlignment
		)
		(vla-SetAlignment reportTable
			acTitleRow ;;rowTypes
			acBottomLeft ;;cellAlignment
		)
		(vla-SetTextHeight reportTable
			acDataRow ;;rowTypes
			(/ 12.0 72) ;;textHeight
		)
		(vla-SetTextHeight reportTable
			acHeaderRow ;;rowTypes
			(/ 12.0 72) ;;textHeight
		)
		(vla-SetTextHeight reportTable
			acTitleRow ;;rowTypes
			(/ 12.0 72) ;;textHeight
		)
		
		; (vla-SetColumnWidth reportTable
			; 0 ;;columnIndex
			; 0.95 ;;width
		; )
		(vla-SetColumnWidth reportTable
			0 ;;columnIndex
			(+ 3.15 0.95) ;;width
		)
		(vla-SetColumnWidth reportTable
			1 ;;columnIndex
			2 ;;width
		)
		
		(foreach rowIndex (range (vla-get-rows reportTable))
			(vla-setRowHeight reportTable rowIndex 0.001) ;;we set the row height to almost zero, so that the natural height of the text determines the row height.
		)
		;;right-align the column of numbers
		(foreach rowIndex (cdr (range (vla-get-rows reportTable))) ;;for all rows after the first row
			(vla-setCellAlignment reportTable 
				rowIndex ;;rowIndex
				1 ;;columnindex
				acTopRight
			)
		)

		(fullyPurgeDocument grandTotalsReportDrawing)
		
		(setq pathOfGrandTotalsReportDrawing 
			(strcat 
				(cdr (assoc "outputDirectory" argument)) "\\" 
				"allBuildings"
				"-report"
				".dwg"
			)
		)
		(vl-file-delete pathOfGrandTotalsReportDrawing) ;remove leftover grandTotalsReportDrawing from a previous running, if it exists, so that the following saveAs will succeed.
		(vla-SaveAs grandTotalsReportDrawing pathOfGrandTotalsReportDrawing)
		(vla-Close grandTotalsReportDrawing)
		(vlax-release-object grandTotalsReportDrawing)
		;;attempt to delete the *.bak file that might have been created alongside the grandTotalsReportDrawing file.
		(vl-file-delete 		
			(strcat 
				(substr pathOfGrandTotalsReportDrawing  1 (- (strlen pathOfGrandTotalsReportDrawing) (strlen (vl-filename-extension pathOfGrandTotalsReportDrawing))))
				".bak"
			)
		)
		
	)
	
	
	(vl-file-delete pathOfWorkingDrawing) ;remove leftover working drawing
	
	
	;;update the value of the grandTotals symbol that was passed as an argument.
	;;(set (cdr (assoc "grandTotals" argument)) grandTotals)
	(princ)
)




;;processDocuments applies processDocument() to each element of arguments,
;; and collects grand totals of area, and then generates the allBuildings report.
(defun processDocuments (arguments /
		grandTotals
	)
	(setq grandTotals (list))
	
	;; stick a "grandTotals" field in each element of arguments.  the value of this field will be a symbol, that processDocument will update accordingly.
	(setq arguments
		(mapcar
			'(lambda (x)
				;;remove any "grandTotals" field that might already be in x
				(setq x
					(vl-remove
						(assoc "grandTotals" x)
						x
					)
				)
				; add our "grandTotals" field to x.
				(append x (list  (cons "grandTotals" 'grandTotals)))
			)
			arguments
		)
	)
	
	(mapcar 'processDocument arguments)
	
	(writeToLog "grandTotals: ")(writeToLog grandTotals)(writeToLog "\n")
	
)
(princ)
















