; ;;(vlax-dump-object (vlax-ename->vla-object (car (entsel))))
;;  (dumpallproperties (car (entsel)))

(defun centerDimensionText (dimension / 
		newtextpoint
		initialDimConstrForm
	)
	(setq initialDimConstrForm (vla-get-DimConstrForm dimension))
	(setq newtextpoint (mapcar '(lambda (x y) (/ (+ x y) 2))
				(gc:VariantToLispData (vla-get-ExtLine1Point dimension))
				(gc:VariantToLispData (vla-get-ExtLine2Point dimension))
			)
	)
	;;(princ "setting text position to            ")(princ newtextpoint)(princ "\n")
	(vla-put-DimConstrForm dimension :vlax-false)
	(if 
		(vl-catch-all-error-p 
			(vl-catch-all-apply 'vla-put-TextPosition 
				(list 
					dimension
					(vlax-3d-point 
						newtextpoint
					)
				)
			) 
		)
		(progn 
			(princ "anomaly occured while setting text position.\n")
		)
	)
	(vla-get-DimConstrForm dimension) ;;not sure why, but this seems to be necessary to make the following 'put' behave itself.
	(vla-put-DimConstrForm dimension initialDimConstrForm)
)

(defun C:resetAllDimensionTextPosition ()
	(vlax-for entity 
		(vla-get-ModelSpace (vla-get-ActiveDocument (vlax-get-acad-object)));; 
		;;(vla-item (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) "level1-building2")
		(if 
			(or
				(= "AcDbAlignedDimension" (vla-get-ObjectName entity))
				;;(= "AcDbRotatedDimension" (vla-get-ObjectName entity))
			)
			(progn
				(princ (vla-get-Handle entity))(princ ": ")(princ (vla-get-ObjectName entity)) 
				(princ " ")(princ (vla-get-DimConstrName entity ))
				(princ "=")(princ (vla-get-DimConstrExpression entity ))  
				(princ " ")(princ (if (= :vlax-true (vla-get-DimConstrReference entity )) " (reference) " ""))
				(princ " ")(princ (if (= :vlax-true (vla-get-DimConstrForm entity )) " (constraint form) " ""))
				(princ " ")(princ "owner: ")
				(princ 
					(vla-get-Name 
						(vla-ObjectIDToObject (vla-get-ActiveDocument (vlax-get-acad-object)) 
							(vla-get-OwnerID entity)
						) 	
					)
				)
				(princ "\n")
				(if (vl-catch-all-error-p (vl-catch-all-apply 'centerDimensionText (list entity)) )
					(progn 
						(princ "failed to set text position.\n")
					)
				)
				(princ)
			)
		)
	)
)


