;; Round Multiple  -  Lee Mac
;; Rounds 'n' to the nearest multiple of 'm'
(defun LM:roundm ( n m )
	(* m (fix ((if (minusp n) - +) (/ n (float m)) 0.5)))
)


;; snapLeaderAngle assumes that mLeader is a leader with a single leader line. 
;; snapLeaderAngle moves the end of the leader arrow nearest the text to a point that preserves the original distance of the leader line, but 
;; makes the angle of the leader line change to the closest multiple of 15 degrees.
(defun snapLeaderAngle (mLeader / 
		leaderClusterIndex
		leaderLineIndex
		leaderLineIndexes
		leaderLineVertices
		firstVertex
		lastVertex
		currentAngle
		targetAngle
		targetLastVertex
		lineLength
	)



	
	(if (=  (vla-get-LeaderCount mLeader) 1)
		(progn
			; (setq leaderClusterIndex 0) ; valid values are in the range [0, mLeader.LeaderCount-1] (this does not seem to be quite true - it almost seems like the clusterIndex corresponds to which of the 6 or so attachment points the leader cl;uster could have around the text object.)
			;; we will simply try clusterIndices 0 through 50 and take the first one that works.
			(setq leaderClusterIndex 0)
			(setq maxLeaderClusterIndicesToTry 50)
			(while 
				(and 
					(< leaderClusterIndex maxLeaderClusterIndicesToTry) 
					(vl-catch-all-error-p 
						(setq leaderLineIndexes
							(vl-catch-all-apply 'vla-GetLeaderLineIndexes (list  mLeader leaderClusterIndex))
						)
					)
				)
				(setq leaderClusterIndex (+ leaderClusterIndex 1))
			)
			(princ "leaderClusterIndex: ")(princ leaderClusterIndex)(princ "\n")
			
			(if (not (vl-catch-all-error-p leaderLineIndexes))
				(progn
					(setq leaderLineIndexes (gc:VariantToLispData leaderLineIndexes))
					(princ "leaderLineIndexes: " )(princ leaderLineIndexes)(princ "\n")
					(if (= (length leaderLineIndexes) 1)
						(progn
							(setq leaderLineIndex (nth 0 leaderLineIndexes))
							(setq leaderLineVertices (gc:VariantToLispData (vla-GetLeaderLineVertices mLeader leaderLineIndex)))
							(if (= (length leaderLineVertices) 6)
								(progn
									(princ "leaderLineIndex: ")(princ leaderLineIndex)(princ "\n")
									(princ "leaderLineVertices: ")(princ leaderLineVertices)(princ "\n") ;; the order of the vertices is from the arrow tip to the text.
									(setq lastVertex 
										(list 
											(nth (- (length leaderLineVertices) 3)  leaderLineVertices)
											(nth (- (length leaderLineVertices) 2)  leaderLineVertices)
											(nth (- (length leaderLineVertices) 1)  leaderLineVertices)
										)
									)
									(setq firstVertex 
										(list 
											(nth 0  leaderLineVertices)
											(nth 1  leaderLineVertices)
											(nth 2 leaderLineVertices)
										)
									)
									
									(setq currentAngle 
										(atan 
											(- (nth 1 lastVertex) (nth 1 firstVertex) )
											(- (nth 0 lastVertex) (nth 0 firstVertex) )
										)
									)
									
									 ;; we will leave z coords untouched.
									(setq lineLength
										(sqrt
											(+
												(expt (- (nth 0 lastVertex) (nth 0 firstVertex) ) 2)
												(expt (- (nth 1 lastVertex) (nth 1 firstVertex) ) 2)
												;(expt (- (nth 2 lastVertex) (nth 2 firstVertex) ) 2)
											)
										)	
									)
									
									
									(setq targetAngle (LM:roundm currentAngle (/ PI 12)))
									(setq targetLastVertex 
										(append
											(mapcar '+
												(list 
													(nth 0 firstVertex)
													(nth 1 firstVertex)
												)
												(list
													(* lineLength (cos targetAngle) )
													(* lineLength (sin targetAngle) )
												)
											)
											(list
												(nth 2 lastVertex)	
											)
										)
									)
									(princ "targetLastVertex: " )(princ targetLastVertex)(princ "\n")
									
									(vla-SetLeaderLineVertices mLeader leaderLineIndex 
										(gc:3dPointListToVariant
											(list firstVertex targetLastVertex)
										)
									)
								)
								(progn
									(princ "skipping this mLEader because the leader line has more than two vertices. \n")
								)
							)
						)
						(progn
							(princ "skipping this mLEader because the leader cluster has more than one leader line. \n")
						)
					)
				)
				(progn
					(princ "skipping this mLEader because no valid leaderClusterIndex could be found. \n")
				)
			)
		)
		(progn
			(princ "skipping this mLeader because it has more than one leader cluster.\n")
		)
	)
	
	(princ)
)




;; getFirstVertexOfMleader() returns a triple of reals that are the coordinates in world space of the tip 
;; of one of the arrows of the specified mLeader.  Returns nil in case of exception (i.e. if no arrow tip
;; point can be found).
(defun getFirstVertexOfMleader (mLeader /
		leaderClusterIndex
		leaderLineIndex
		leaderLineIndexes
		leaderLineVertices
		firstVertex
		lastVertex
		currentAngle
		targetAngle
		targetLastVertex
		lineLength
	)
	
	(if (=  (vla-get-LeaderCount mLeader) 1)
		(progn
			; (setq leaderClusterIndex 0) ; valid values are in the range [0, mLeader.LeaderCount-1] (this does not seem to be quite true - it almost seems like the clusterIndex corresponds to which of the 6 or so attachment points the leader cl;uster could have around the text object.)
			;; we will simply try clusterIndices 0 through 50 and take the first one that works.
			(setq leaderClusterIndex 0)
			(setq maxLeaderClusterIndicesToTry 50)
			(while 
				(and 
					(< leaderClusterIndex maxLeaderClusterIndicesToTry) 
					(vl-catch-all-error-p 
						(setq leaderLineIndexes
							(vl-catch-all-apply 'vla-GetLeaderLineIndexes (list  mLeader leaderClusterIndex))
						)
					)
				)
				(setq leaderClusterIndex (+ leaderClusterIndex 1))
			)
			(princ "leaderClusterIndex: ")(princ leaderClusterIndex)(princ "\n")
			
			(if (not (vl-catch-all-error-p leaderLineIndexes))
				(progn
					(setq leaderLineIndexes (gc:VariantToLispData leaderLineIndexes))
					(princ "leaderLineIndexes: " )(princ leaderLineIndexes)(princ "\n")
					(if (= (length leaderLineIndexes) 1)
						(progn
							(setq leaderLineIndex (nth 0 leaderLineIndexes))
							(setq leaderLineVertices (gc:VariantToLispData (vla-GetLeaderLineVertices mLeader leaderLineIndex)))
							(if (= (length leaderLineVertices) 6)
								(progn
									(princ "leaderLineIndex: ")(princ leaderLineIndex)(princ "\n")
									(princ "leaderLineVertices: ")(princ leaderLineVertices)(princ "\n") ;; the order of the vertices is from the arrow tip to the text.
									(setq lastVertex 
										(list 
											(nth (- (length leaderLineVertices) 3)  leaderLineVertices)
											(nth (- (length leaderLineVertices) 2)  leaderLineVertices)
											(nth (- (length leaderLineVertices) 1)  leaderLineVertices)
										)
									)
									(setq firstVertex 
										(list 
											(nth 0  leaderLineVertices)
											(nth 1  leaderLineVertices)
											(nth 2 leaderLineVertices)
										)
									)
								)
								(progn
									(princ "skipping this mLEader because the leader line has more than two vertices. \n")
								)
							)
						)
						(progn
							(princ "skipping this mLEader because the leader cluster has more than one leader line. \n")
						)
					)
				)
				(progn
					(princ "skipping this mLEader because no valid leaderClusterIndex could be found. \n")
				)
			)
		)
		(progn
			(princ "skipping this mLeader because it has more than one leader cluster.\n")
		)
	)
	firstVertex
)


(defun drawAngularSnapLines (point space numberOfDivisions /
		temporaryLayer
		thisRay
		angle
		angles
		numberOfDivisions
		initialActiveLayer
	)
	(setq initialActiveLayer (vla-get-ActiveLayer (vla-get-Document space)))
	(setq temporaryLayer 
		(vla-Add 
			(vla-get-Layers (vla-get-Document space))
			(GUID )
		)
	)
	(vla-put-Lock temporaryLayer :vlax-false)
	(vla-put-ActiveLayer (vla-get-Document space) temporaryLayer)
	

	(setq angles
		(mapcar	
			'(lambda (x) (* (/ (float x) numberOfDivisions) (* 2 PI)))
			(range numberOfDivisions)
		)
	)
	;(princ angles)
	(foreach angle angles
		(if 
			(not 
				(vl-catch-all-error-p
					(setq thisRay 
						(vl-catch-all-apply 'vla-addRay 
							(list 
								space 
								(vlax-3D-point  point)
								(vlax-3D-point  
									(mapcar '+
										point
										(list
											(cos angle)
											(sin angle)
											0
										)
									)
								)
							)
						)
					)
				)
			)
			(progn
				(appendTo 'temporaryEntities thisRay)
				(vla-put-Layer thisRay (vla-get-Name temporaryLayer))
				(vla-put-Lineweight thisRay acLnWt000)
				(vla-put-TrueColor thisRay (newColor (list 238 205 230))) ;;this is a pale violet color
				
				;set color and layer of thisRay
			)
		)
	)


	
	(vla-put-ActiveLayer (vla-get-Document space) initialActiveLayer)
	;;in case we didn't end up creating any entities, we should delete the temporaryLayer (this will fail silently if there actually are entites on the temporary layer)
	(vl-catch-all-apply 'vla-delete (list  temporaryLayer))
	(vl-catch-all-apply 'vla-put-Lock (list  temporaryLayer :vlax-true))
)
(defun C:drawAngularSnapLines ( / 
		point
		space
		numberOfDivisions
	)
	(setq space 
		(if (= acModelSpace (vla-get-ActiveSpace (vla-get-ActiveDocument (vlax-get-acad-object))))
			(vla-get-ModelSpace (vla-get-ActiveDocument (vlax-get-acad-object)))
			(if (= acPaperSpace (vla-get-ActiveSpace (vla-get-ActiveDocument (vlax-get-acad-object))))
				(vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object)))
				nil
			)
		)
	)
	(setq numberOfDivisions 24)
	(princ "select a point: ")
	(setq point (gc:VariantToLispData (vla-GetPoint (vla-get-Utility (vla-get-ActiveDocument (vlax-get-acad-object)) ))))
	(princ point)
	; (princ "point: ")(princ point)(princ "\n")
	; (princ "space: ")(princ space)(princ "\n")
	(removeTemporaryEntities ) ;get rid of any old temporaryEntities that might be left over from the last running.
	(drawAngularSnapLines 
		point ;;point
		space ;;space
		numberOfDivisions ;; numberOfDivisions
	)
	(princ)
)


(defun drawLeaderSnapLines (mLeader /
		firstVertex
		temporaryLayer
		space ;;the block definition that contains the mLeader.  We will add the temporary lines to space.
		;temporaryEntities
		thisRay
		angle
		angles
		numberOfDivisions
		initialActiveLayer
	)
	(setq numberOfDivisions 24)

	(setq space 
		(vla-ObjectIDToObject (vla-get-Document mLeader) (vla-get-OwnerID mLeader))
	)
	(if (setq firstVertex (getFirstVertexOfMleader mLeader))
		(progn
			(drawAngularSnapLines firstVertex space numberOfDivisions)
		)
	)

)

(defun C:drawLeaderSnapLines ( / 
		mLeader
	)

	(if 
	(not 
		(vl-catch-all-error-p 
			(setq mLeader 
				(vl-catch-all-apply 'vlax-ename->vla-object (list (car (entsel "Select an MLeader"))))
			)
			
		)
	)
		(progn
			(if (/= (vla-get-ObjectName mLeader) "AcDbMLeader")
				(progn
					(princ "\n")(princ "Oops.  You seem to have selected an ")(princ (vla-get-ObjectName mLeader))(princ ", but we were expecting an ")(princ "AcDbMLeader")(princ ".  Goodbye.")(princ "\n")
				)
				(progn
					(removeTemporaryEntities ) ;get rid of any old temporaryEntities that might be left over from the last running.
					(drawLeaderSnapLines mLeader)
					(princ "You may invoke \"removeTemporaryEntities\" to delete the snap lines, when you are finished using them.")
				)
			)
		)
	)
	(princ)

)


;; removeTemporaryEntities relies on the existence of a global variable named temporaryEntities, which is a list of objects, which was populated by (drawLeaderSnapLines)
(defun removeTemporaryEntities ( / 
		temporaryEntity
		layerToBeDeleted
		document
		intitialLockState
	)
	(princ "number of temporary entities: " )(princ (length temporaryEntities))(princ "\n")
	(foreach temporaryEntity temporaryEntities
		(setq document            (vl-catch-all-apply 'vla-get-Document  (list temporaryEntity)))
		(setq layerToBeDeleted    
			(vl-catch-all-apply 'vla-item          
				(list 
					(vl-catch-all-apply 'vla-get-Layers (list document)) 
					(vl-catch-all-apply 'vla-get-Layer (list temporaryEntity)) 
				)
			)
		)
		(setq intitialLockState   (vl-catch-all-apply 'vla-get-Lock      (list layerToBeDeleted)))
		(vl-catch-all-apply 'vla-put-Lock (list layerToBeDeleted :vlax-false))
		(vl-catch-all-apply 'vla-delete (list temporaryEntity))
		(vl-catch-all-apply 'vla-delete (list layerToBeDeleted ))
		(vl-catch-all-apply 'vla-put-Lock (list  temporaryLayer intitialLockState))
	)
	(setq temporaryEntities nil)
	(princ)
)

(defun C:removeTemporaryEntities ( / 
	)
	(removeTemporaryEntities)
	(princ)
)



; (snapLeaderAngle (vlax-ename->vla-object (car (entsel))))
; (vlax-dump-object (vlax-ename->vla-object (car (entsel))))
(princ "\n")
; (snapLeaderAngle (vla-handleToObject (vla-get-ActiveDocument (vlax-get-acad-object)) "34749"))
;; (snapLeaderAngle (vlax-ename->vla-object (car (entsel))))
;;(vlax-dump-object (vla-handleToObject (vla-get-ActiveDocument (vlax-get-acad-object)) "34749"))

; (vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))
	; (vlax-for entity blockDefinition
		; (if (= (vla-get-ObjectName entity) "AcDbMLeader")
			; (progn
				; (princ "now processing mLeader ")(princ "\"")(princ (vla-get-TextString entity))(princ "\"") (princ " in ")(princ (vla-get-Name blockDefinition))(princ ".")(princ "\n")
				; (snapLeaderAngle entity)
			; )
		; )
	; )					
; )

(princ)

; (setq myMLeader  (vlax-ename->vla-object (car (entsel))))
; (princ (vla-GetLeaderIndex myMLeader 0))(princ)
; (princ (gc:VariantToLispData (vla-GetLeaderLineIndexes myMLeader 1)))(princ "\n")(princ)
;;(vlax-dump-object myMLeader)