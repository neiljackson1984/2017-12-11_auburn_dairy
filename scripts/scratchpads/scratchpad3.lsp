
(setq numRows 4)
(setq numColumns 3)
(setq reportTable 
	(vla-AddTable (vla-get-ModelSpace (vla-get-ActiveDocument (vlax-get-acad-object)))
		(vlax-3D-point (list 0 0 0)) ;;insertion point
		numRows ;;numRows
		numColumns ;;numColumns
		0.01 ;; rowHeight
		2 ;; column width
	)
)



(foreach rowIndex (range numRows )
	(princ 
		(strcat 
			"rowType: " 
			(COND
				(list
					(= acDataRow (vla-GetRowType reportTable rowIndex))
					"acDataRow"
				)
				(list
					(= acHeaderRow (vla-GetRowType reportTable rowIndex))
					"acHeaderRow"
				)
				(list
					(= acTitleRow (vla-GetRowType reportTable rowIndex))
					"acTitleRow"
				)
				(list
					(= acUnknownRow (vla-GetRowType reportTable rowIndex))
					"acUnknownRow"
				)
				(list
					T
					(vla-GetRowType reportTable rowIndex)
				)
			)
		)
	)
	(princ "\n")

	(foreach columnIndex (range numColumns)
		; (setq ff 
			; '(lambda (x)
				; (if 
					; (= 
						; :vlax-true 	
						; (vla-GetCellGridVisibility reportTable 
							; rowIndex
							; columnIndex
							; acLeftMask
						; )
					; )
					; "1"
					; "0"
				; )
			; )
		; )
		
		; (setq gridVisibilityData 
			; (mapcar 
				; ff
				; (list acLeftMask acTopMask acRightMask acBottomMask)
			; )
		; )
		
		; (setq gridVisibilityReportString 
			; (apply 'strcat
				; gridVisibilityData
			; )
		; )
		
	
		(setq gridVisibilityReportString 
			(apply 'strcat
				(mapcar 
					'(lambda (x) 
						(if
							(= 
								:vlax-true 
								(vla-GetCellGridVisibility reportTable 
									rowIndex
									columnIndex
									x
								)
							)
							"1"
							"0"
						)
					)
					(list acLeftMask acTopMask acRightMask acBottomMask)
				)
			)
		)
		
		(vla-setTextString reportTable 
			rowIndex
			columnIndex
			0
			"bsafasdfadsf"
		)
		(vla-setText reportTable 
			rowIndex
			columnIndex
			"tghjghfj"
		)
		(vla-setText reportTable 
			rowIndex
			columnIndex
			(strcat 
				(strcat "ahoy " (itoa rowIndex) "-" (itoa columnIndex) "\n")
				gridVisibilityReportString "\n"
				(COND
					(list 
						(= acCellContentTypeBlock (vla-GetContentType reportTable rowIndex columnIndex))
						"acCellContentTypeBlock"
					)
					(list
						(= acCellContentTypeField (vla-GetContentType reportTable rowIndex columnIndex))
						"acCellContentTypeField"
					)
					(list
						(= acCellContentTypeUnknown (vla-GetContentType reportTable rowIndex columnIndex))
						"acCellContentTypeUnknown"
					)
					(list
						(= acCellContentTypeValue (vla-GetContentType reportTable rowIndex columnIndex))
						"acCellContentTypeValue"
					)
					(list
						T
						(itoa (vla-GetContentType reportTable rowIndex columnIndex))
					)
				)
			)
		)
		(setq contentId (vla-CreateContent reportTable rowIndex columnIndex 3))
		(vla-setTextString reportTable 
			rowIndex
			columnIndex
			1
			(strcat "xxx" (itoa contentId))
		)
		(setq contentId (vla-CreateContent reportTable rowIndex columnIndex 16))
		(vla-setTextString reportTable 
			rowIndex
			columnIndex
			contentId
			(strcat "xxx" (itoa contentId))
		)
		
		(setq contentId (vla-CreateContent reportTable rowIndex columnIndex 16))
		(vla-setTextString reportTable 
			rowIndex
			columnIndex
			contentId
			(strcat "GetCellBackgroundColorNone" (if (= :vlax-true (vla-GetCellBackgroundColorNone reportTable rowIndex columnIndex)) ":vlax-true" ":vlax-false"))
		)
		
		(princ "column name: ")(princ (vla-GetColumnName reportTable columnIndex))(princ "\n")
		
		(vla-RemoveAllOverrides reportTable rowIndex columnIndex)
		
		; (princ (vla-getText reportTable rowIndex columnIndex)) (princ "\n")
	)
)

(vla-SetAlignment reportTable
	acDataRow ;;rowTypes
	acTopLeft ;;cellAlignment
)
(vla-SetAlignment reportTable
	acHeaderRow ;;rowTypes
	acTopLeft ;;cellAlignment
)
(vla-SetAlignment reportTable
	acTitleRow ;;rowTypes
	acTopLeft ;;cellAlignment
)

(setq rowIndex 1)
(setq columnIndex 2)
(setq color 
	(vla-GetCellBackgroundColor reportTable 
		rowIndex
		columnIndex
	) 
)

(vla-SetRGB color
	0    ;;red
	155  ;;green
	0    ;;blue
)

(vla-SetCellBackgroundColor reportTable 
	rowIndex
	columnIndex
	color
) 


(vla-SetCellBackgroundColor reportTable 
	(+ 1 rowIndex)
	columnIndex
	(newColor '(155 0 0))
) 

(vla-SetCellBackgroundColor reportTable 
	(+ 1 rowIndex)
	columnIndex
	(newColor '(0 0 180))
) 

;;(mapcar 'princ (list acDataRow acHeaderRow acTitleRow acUnknownRow))


; (vla-UnmergeCells reportTable 
	; 0;;minRow
	; 0;;maxRow
	; 0;;minCol
	; 2;;maxCol
; )




; (vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))
	; (princ (vla-get-Name blockDefinition))(princ "\n")
; )
(princ)

