
(setq pathOfLayoutFixerScript "C:\\work\\2017-12-11 auburn dairy\\scripts\\scratchpads\\scratchpad7 - viewport settings.lsp")
(setq pathOfScriptFile  (vl-filename-mktemp nil nil ".lsp"))
(setq scriptFile (open pathOfScriptFile "w"))
(write-line 
	(strcat 
		;;"(command-s \"line\" (list 0 0) (list 1 1) (list 1 0) \"close\" )" "\n"  ;;this draws a triangle.
		"(load \"" (addslashes pathOfLayoutFixerScript) "\")" "\n" ;; I am relying on the document opening that occurs within executeScriptOnDrawing to load the usual acaddoc.lsp, so that the needed libraries will be available.
		"(command-s \"qsave\")" "\n" ;;save the document
		"(command-s \"close\")" "\n" ;;close the document (probably necessary in order to alloow the invoking script to continue (due to SendCommand behavior of activating the target document, thus pausing scripts in another document.)
	)
	scriptFile
)
(close scriptFile)



(setq pathOfDocumentsDirectory  "C:\\work\\2017-12-11 auburn dairy\\sheets")
(setq pathsOfDrawingFiles 
	(mapcar
		'(lambda (x) (findfile (strcat pathOfDocumentsDirectory "\\" x)))
		(vl-directory-files pathOfDocumentsDirectory "*.dwg")
	)
)

(foreach pathOfDrawingFile pathsOfDrawingFiles
	(executeScriptOnDrawing pathOfScriptFile pathOfDrawingFile)
)


