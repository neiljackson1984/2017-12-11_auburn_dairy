; (vlax-dump-object (vlax-ename->vla-object (car (entsel))) )

; (vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))
	; (vlax-for entity blockDefinition
		; (setq linetype (vl-catch-all-apply 'vla-get-LineType (list entity)))
		; (princ "found a ")(princ  (vla-get-ObjectName entity))(princ "(handle: ")(princ (vla-get-Handle entity))(princ ") ")(princ " in ")(princ (vla-get-Name blockDefinition))(princ ". ")(princ "linetype: ")(princ linetype)(princ ".")(princ "\n")
		; (if (= "PHANTOM2" (setq linetype (vl-catch-all-apply 'vla-get-LineType (list entity))))
			; (progn
				; (princ "found a ")(princ  (vla-get-ObjectName entity))(princ "(handle: ")(princ (vla-get-Handle entity))(princ ") ")(princ " in ")(princ (vla-get-Name blockDefinition))(princ ".") (princ "\n")
			; )
		; )
	; )
; )

; (vlax-for layer (vla-get-Layers (vla-get-ActiveDocument (vlax-get-acad-object)))
	; (princ (vla-get-Name layer))
; )

; (vlax-for dictionary (vla-get-Dictionaries (vla-get-ActiveDocument (vlax-get-acad-object) ))
	; ; strangely, the Dictionaries collection contains some items that are note dictionaries (i.e.  ObjectName != "AcDbDictionary" and do not have a 'Name' property.)
	; (if (= "AcDbDictionary" (vla-get-ObjectName dictionary))
		; (progn
			; (princ (vl-catch-all-apply 'vla-get-Name (list dictionary)))(princ "\n")
			; (vl-catch-all-apply 'vla-Delete (list dictionary))
		; )
	; )
; )

; (princ "\n")
; (princ (vla-get-LinearScaleFactor (setq entity (vlax-ename->vla-object (car (entsel)))))) (princ "\n")
; (princ (getpropertyvalue (vlax-vla-object->ename entity) "Associative"))
; (princ)
; (vlax-dump-object (vlax-ename->vla-object (car (entsel))))
; (vla-put-LinearScaleFactor (vlax-ename->vla-object (car (entsel))) -1)

; (dumpallproperties (car (entsel)))

; (princ (cdr (assoc (vla-get-StandardScale (vla-get-Layout (vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object))))) acPlotScale_enumValues)))
	
(vlax-for entity (vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object)))
	(if (= "AcDbRotatedDimension" (vla-get-ObjectName entity))
		(progn
			(setq initialLinearScaleFactor (vla-get-LinearScaleFactor entity))
			(vla-put-LinearScaleFactor entity 1)
			(setq finalLinearScaleFactor (vla-get-LinearScaleFactor entity))
			(princ "Changed the LinearScaleFactor of ")(princ (vla-get-Handle entity))(princ " (")
			(princ 
				(if (= (getpropertyvalue (vlax-vla-object->ename entity) "Associative") 1) "    associative" "not associative")
			)
			(princ ")")(princ " from ")(princ initialLinearScaleFactor)(princ " to ")(princ finalLinearScaleFactor)(princ ".")(princ "\n")
		)
	)
)

(vl-catch-all-apply 'vla-add (list (vla-get-Layers (vla-get-ActiveDocument (vlax-get-acad-object))) "temp2"))
(vla-put-Color (vla-item (vla-get-Layers (vla-get-ActiveDocument (vlax-get-acad-object))) "temp2") 220)


(vlax-for entity (vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object)))
	(if (= "AcDbRotatedDimension" (vla-get-ObjectName entity))
		(if (= (getpropertyvalue (vlax-vla-object->ename entity) "Associative") 1)
			(progn
				(if (= (vla-get-Layer entity) "temp2")
					(progn
						(vla-put-Layer entity "0")
					)
				)
			)
			(progn
				(princ "found a non-associative dimension (" )(princ (vla-get-Handle entity))(princ  ")")(princ "\n")
				(vla-put-Layer entity "temp2")
			)
		)
	)
)

