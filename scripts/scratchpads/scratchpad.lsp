(princ "\n")
(if (not myBlockReference) (setq myBlockReference (vlax-ename->vla-object (car (entsel)))))
;;(setq myBlockReference (vlax-ename->vla-object (car (entsel))))
;;(vlax-dump-object myBlockReference)
;;(princ ( LM:getdynprops myBlockReference))(princ "\n")
;;(if (= (vla-get-IsDynamicBlock myBlockReference) :vlax-true) (princ "it is dynamic") (princ "it is not dynamic"))

;; %<\_FldPtr 1920811152912>%


;;(setq properties ( LM:getdynprops myBlockReference))
;;(setq properties (list (cons "length"  2.2) (cons "cameraDirectionAngle"  1.65806)))
;;(LM:setdynprops myBlockReference properties)

;; {\C1;ahoy %<\AcSm Sheet.Number>%  }
;;{\C1;ahoy %<\_FldPtr 1920642378224>%  }
(foreach attributeReference (gc:VariantToLispData (vla-GetAttributes myBlockReference))
	(princ "attributeReference.TagString: ")(princ (vla-get-TagString attributeReference))(princ "\n")
	(princ "\t")(princ "attributeReference.TextString: ")(princ (vla-get-TextString attributeReference))(princ "\n")
	(princ "\t")(princ "attributeReference.MTextAttributeContent: ")(princ (vla-get-MTextAttributeContent attributeReference))(princ "\n")
	(princ "\t")(princ "fieldcode: ")(princ (LM:fieldcode (vlax-vla-object->ename attributeReference)))(princ "\n")
	
	;;(princ "fieldObjects: ")(princ (LM:fieldobjects (vlax-vla-object->ename attributeReference)))(princ "\n")
	
	;;(vla-UpdateMTextAttribute attributeReference)
)

(princ)

; (princ (vla-get-TextString (vlax-ename->vla-object (car (entsel)))))(princ)
; (princ (LM:fieldobjects (car (entsel))))(princ)
(princ (LM:fieldcode (car (entsel))))(princ)


(defun LM:fieldobjects ( ent / _getfieldobjects )

    (defun _getfieldobjects ( a )
        (apply 'append
            (mapcar
               '(lambda ( a )
                    (if (= 360 (car a))
                        (_getfieldobjects (cdr a))
                        (if (= 331 (car a)) (list (cdr a)))
                    )
                )
                (entget a)
            )
        )
    )
    
    (if (and (wcmatch  (cdr (assoc 0 (setq ent (entget ent)))) "TEXT,MTEXT,ATTRIB")
             (setq ent (cdr (assoc 360 ent)))
             (setq ent (dictsearch ent "acad_field"))
             (setq ent (dictsearch (cdr (assoc -1 ent)) "text"))
        )
        (_getfieldobjects (cdr (assoc -1 ent)))
    )
)

; (vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))
	; (vlax-for entity blockDefinition
		; (if (= (vla-get-Layer entity) "temp")
			; (progn
				; (princ "found a ")(princ (vla-get-ObjectName entity))(princ " on layer ") (princ (vla-get-Layer entity))(princ " in")(princ (vla-get-Name blockDefinition))(princ ".")(princ "\n")
				; ; (if 
					; ; (vl-catch-all-error-p 
						; ; (setq e 
							; ; (vl-catch-all-apply 'vla-put-Layer (list entity "0"))
						; ; )
					; ; )
					; ; (progn 
						; ; (princ "failed to set its layer to 0.  error message: ")(princ (vl-catch-all-error-message e))(princ "\n")
					; ; )
				; ; )
			; )
		; )
	; )
; )


; (vlax-for blockDefinition (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))
	; (princ (Vla-get-Name blockDefinition))(princ "\n")
	; (vlax-for entity blockDefinition
		; (if (/= (vla-get-Layer entity) "0")
			; (progn
				; (princ "found a ")(princ (vla-get-ObjectName entity))(princ " on layer ") (princ (vla-get-Layer entity))(princ " in")(princ (vla-get-Name blockDefinition))(princ ".")(princ "\n")
				; (if 
					; (vl-catch-all-error-p 
						; (setq e 
							; (vl-catch-all-apply 'vla-put-Layer (list entity "0"))
						; )
					; )
					; (progn 
						; (princ "failed to set its layer to 0.  error message: ")(princ (vl-catch-all-error-message e))(princ "\n")
					; )
				; )
			; )
		; )
	; )
; )



; (if (not myMtext) (setq myMtext (vlax-ename->vla-object (car (entsel)))))

; (vlax-dump-object myMtext)
; (vla-put-Width myMtext 0)

(princ)