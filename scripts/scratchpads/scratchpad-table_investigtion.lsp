
(progn ;; initialize the table objects
	(setq legendTable nil)
	(setq abbreviationsTable nil)
	(vlax-for entity (vla-get-PaperSpace (vla-get-ActiveDocument (vlax-get-acad-object)))
		(if (= "AcDbTable" (vla-get-ObjectName entity))
			(progn
				(if 
					(= 
						(vl-catch-all-apply 'strcase 
							(list 
								(vl-catch-all-apply 'vlax-variant-value 
									(list 
										(vla-GetValue  entity 
											0	;;rowIndex 
											0	;;columnIndex 
											0	;;contentIndex
										)
									)
								)
							)
						)
						(strcase "legend")
					)
					(progn
						(setq legendTable entity)
					)
				)
				(if 
					(= 
						(vl-catch-all-apply 'strcase 
							(list 
								(vl-catch-all-apply 'vlax-variant-value 
									(list 
										(vla-GetValue  entity 
											0	;;rowIndex 
											0	;;columnIndex 
											0	;;contentIndex
										)
									)
								)
							)
						)
						(strcase "abbreviations")
					)
					(progn
						(setq abbreviationsTable entity)
					)
				)
			)
		)
	)
)

(defun processTable (myTable /
	myTableStyle
	)
	(setq myTableStyle 
		(vla-item 
			(vla-item (vla-get-Dictionaries (vla-get-Document myTable)) "ACAD_TABLESTYLE")
			(vla-get-StyleName myTable)
		)
	)

	(progn ;; define some enum values

		(setq AcCellProperty_enumValues
			(list
				(cons acAlignmentProperty        "acAlignmentProperty")
				(cons acAllCellProperties        "acAllCellProperties")
				(cons acAutoScale                "acAutoScale")
				(cons acBackgroundColor          "acBackgroundColor")
				(cons acBitProperties            "acBitProperties")
				(cons acContentColor             "acContentColor")
				(cons acContentLayout            "acContentLayout")
				(cons acContentProperties        "acContentProperties")
				(cons acDataFormat               "acDataFormat")
				(cons acDataType                 "acDataType")
				(cons acDataTypeAndFormat        "acDataTypeAndFormat")
				(cons acEnableBackgroundColor    "acEnableBackgroundColor")
				(cons acFlowDirBtoT              "acFlowDirBtoT")
				(cons acInvalidCellProperty      "acInvalidCellProperty")
				(cons acLock                     "acLock")
				(cons acMarginBottom             "acMarginBottom")
				(cons acMarginLeft               "acMarginLeft")
				(cons acMarginRight              "acMarginRight")
				(cons acMarginTop                "acMarginTop")
				(cons acMergeAll                 "acMergeAll")
				(cons acRotation                 "acRotation")
				(cons acScale                    "acScale")
				(cons acTextHeight               "acTextHeight")
				(cons acTextStyle                "acTextStyle")
			)
		)

		(setq variantType_enumValues 
			(list
				(cons  vlax-vbEmpty        "vlax-vbEmpty"    )
				(cons  vlax-vbNul          "vlax-vbNul"      )
				(cons  vlax-vbInteger      "vlax-vbInteger"  )
				(cons  vlax-vbLong         "vlax-vbLong"     )
				(cons  vlax-vbSingle       "vlax-vbSingle"   )
				(cons  vlax-vbDouble       "vlax-vbDouble"   )
				(cons  vlax-vbString       "vlax-vbString"   )
				(cons  vlax-vbObject       "vlax-vbObject"   )
				(cons  vlax-vbBoolean      "vlax-vbBoolean"  )
				(cons  vlax-vbArray        "vlax-vbArray"    )

			)
		)
		(setq AcGridLineType_enumValues ;; these appear to be bitmask values
			(list
				(cons acHorzBottom      "acHorzBottom"       )
				(cons acHorzInside      "acHorzInside"       )
				(cons acHorzTop         "acHorzTop"          )
				(cons acInvalidGridLine "acInvalidGridLine"  )
				(cons acVertInside      "acVertInside"       )
				(cons acVertLeft        "acVertLeft"         )
				(cons acVertRight       "acVertRight"        )
			)
		)

		
		(setq AcRowType_enumValues ;; these appear to be bitmask values
			(list
				(cons  acDataRow       "acDataRow"    )
				(cons  acHeaderRow     "acHeaderRow"  )
				(cons  acTitleRow      "acTitleRow"   )
				(cons  acUnknownRow    "acUnknownRow" )
			)
		)
		
		(setq acTableStyleOverrides_enumValues
			(list
				(cons acCellAlign                     "acCellAlign"                     )
				(cons acCellBackgroundColor           "acCellBackgroundColor"           )
				(cons acCellBackgroundFillNone        "acCellBackgroundFillNone"        )
				(cons acCellBottomGridColor           "acCellBottomGridColor"           )
				(cons acCellBottomGridLineWeight      "acCellBottomGridLineWeight"      )
				(cons acCellBottomVisibility          "acCellBottomVisibility"          )
				(cons acCellContentColor              "acCellContentColor"              )
				(cons acCellDataType                  "acCellDataType"                  )
				(cons acCellLeftGridColor             "acCellLeftGridColor"             )
				(cons acCellLeftGridLineWeight        "acCellLeftGridLineWeight"        )
				(cons acCellLeftVisibility            "acCellLeftVisibility"            )
				(cons acCellRightGridColor            "acCellRightGridColor"            )
				(cons acCellRightGridLineWeight       "acCellRightGridLineWeight"       )
				(cons acCellRightVisibility           "acCellRightVisibility"           )
				(cons acCellTextHeight                "acCellTextHeight"                )
				(cons acCellTextStyle                 "acCellTextStyle"                 )
				(cons acCellTopGridColor              "acCellTopGridColor"              )
				(cons acCellTopGridLineWeight         "acCellTopGridLineWeight"         )
				(cons acCellTopVisibility             "acCellTopVisibility"             )
				(cons acDataHorzBottomColor           "acDataHorzBottomColor"           )
				(cons acDataHorzBottomLineWeight      "acDataHorzBottomLineWeight"      )
				(cons acDataHorzBottomVisibility      "acDataHorzBottomVisibility"      )
				(cons acDataHorzInsideColor           "acDataHorzInsideColor"           )
				(cons acDataHorzInsideLineWeight      "acDataHorzInsideLineWeight"      )
				(cons acDataHorzInsideVisibility      "acDataHorzInsideVisibility"      )
				(cons acDataHorzTopColor              "acDataHorzTopColor"              )
				(cons acDataHorzTopLineWeight         "acDataHorzTopLineWeight"         )
				(cons acDataHorzTopVisibility         "acDataHorzTopVisibility"         )
				(cons acDataRowAlignment              "acDataRowAlignment"              )
				(cons acDataRowColor                  "acDataRowColor"                  )
				(cons acDataRowDataType               "acDataRowDataType"               )
				(cons acDataRowFillColor              "acDataRowFillColor"              )
				(cons acDataRowFillNone               "acDataRowFillNone"               )
				(cons acDataRowTextHeight             "acDataRowTextHeight"             )
				(cons acDataRowTextStyle              "acDataRowTextStyle"              )
				(cons acDataVertInsideColor           "acDataVertInsideColor"           )
				(cons acDataVertInsideLineWeight      "acDataVertInsideLineWeight"      )
				(cons acDataVertInsideVisibility      "acDataVertInsideVisibility"      )
				(cons acDataVertLeftColor             "acDataVertLeftColor"             )
				(cons acDataVertLeftLineWeight        "acDataVertLeftLineWeight"        )
				(cons acDataVertLeftVisibility        "acDataVertLeftVisibility"        )
				(cons acDataVertRightColor            "acDataVertRightColor"            )
				(cons acDataVertRightLineWeight       "acDataVertRightLineWeight"       )
				(cons acDataVertRightVisibility       "acDataVertRightVisibility"       )
				(cons acFlowDirection                 "acFlowDirection"                 )
				(cons acHeaderHorzBottomColor         "acHeaderHorzBottomColor"         )
				(cons acHeaderHorzBottomLineWeight    "acHeaderHorzBottomLineWeight"    )
				(cons acHeaderHorzBottomVisibility    "acHeaderHorzBottomVisibility"    )
				(cons acHeaderHorzInsideColor         "acHeaderHorzInsideColor"         )
				(cons acHeaderHorzInsideLineWeight    "acHeaderHorzInsideLineWeight"    )
				(cons acHeaderHorzInsideVisibility    "acHeaderHorzInsideVisibility"    )
				(cons acHeaderHorzTopColor            "acHeaderHorzTopColor"            )
				(cons acHeaderHorzTopLineWeight       "acHeaderHorzTopLineWeight"       )
				(cons acHeaderHorzTopVisibility       "acHeaderHorzTopVisibility"       )
				(cons acHeaderRowAlignment            "acHeaderRowAlignment"            )
				(cons acHeaderRowColor                "acHeaderRowColor"                )
				(cons acHeaderRowDataType             "acHeaderRowDataType"             )
				(cons acHeaderRowFillColor            "acHeaderRowFillColor"            )
				(cons acHeaderRowFillNone             "acHeaderRowFillNone"             )
				(cons acHeaderRowTextHeight           "acHeaderRowTextHeight"           )
				(cons acHeaderRowTextStyle            "acHeaderRowTextStyle"            )
				(cons acHeaderSuppressed              "acHeaderSuppressed"              )
				(cons acHeaderVertInsideColor         "acHeaderVertInsideColor"         )
				(cons acHeaderVertInsideLineWeight    "acHeaderVertInsideLineWeight"    )
				(cons acHeaderVertInsideVisibility    "acHeaderVertInsideVisibility"    )
				(cons acHeaderVertLeftColor           "acHeaderVertLeftColor"           )
				(cons acHeaderVertLeftLineWeight      "acHeaderVertLeftLineWeight"      )
				(cons acHeaderVertLeftVisibility      "acHeaderVertLeftVisibility"      )
				(cons acHeaderVertRightColor          "acHeaderVertRightColor"          )
				(cons acHeaderVertRightLineWeight     "acHeaderVertRightLineWeight"     )
				(cons acHeaderVertRightVisibility     "acHeaderVertRightVisibility"     )
				(cons acHorzCellMargin                "acHorzCellMargin"                )
				(cons acTitleHorzBottomColor          "acTitleHorzBottomColor"          )
				(cons acTitleHorzBottomLineWeight     "acTitleHorzBottomLineWeight"     )
				(cons acTitleHorzBottomVisibility     "acTitleHorzBottomVisibility"     )
				(cons acTitleHorzInsideColor          "acTitleHorzInsideColor"          )
				(cons acTitleHorzInsideLineWeight     "acTitleHorzInsideLineWeight"     )
				(cons acTitleHorzInsideVisibility     "acTitleHorzInsideVisibility"     )
				(cons acTitleHorzTopColor             "acTitleHorzTopColor"             )
				(cons acTitleHorzTopLineWeight        "acTitleHorzTopLineWeight"        )
				(cons acTitleHorzTopVisibility        "acTitleHorzTopVisibility"        )
				(cons acTitleRowAlignment             "acTitleRowAlignment"             )
				(cons acTitleRowColor                 "acTitleRowColor"                 )
				(cons acTitleRowDataType              "acTitleRowDataType"              )
				(cons acTitleRowFillColor             "acTitleRowFillColor"             )
				(cons acTitleRowFillNone              "acTitleRowFillNone"              )
				(cons acTitleRowTextHeight            "acTitleRowTextHeight"            )
				(cons acTitleRowTextStyle             "acTitleRowTextStyle"             )
				(cons acTitleSuppressed               "acTitleSuppressed"               )
				(cons acTitleVertInsideColor          "acTitleVertInsideColor"          )
				(cons acTitleVertInsideLineWeight     "acTitleVertInsideLineWeight"     )
				(cons acTitleVertInsideVisibility     "acTitleVertInsideVisibility"     )
				(cons acTitleVertLeftColor            "acTitleVertLeftColor"            )
				(cons acTitleVertLeftLineWeight       "acTitleVertLeftLineWeight"       )
				(cons acTitleVertLeftVisibility       "acTitleVertLeftVisibility"       )
				(cons acTitleVertRightColor           "acTitleVertRightColor"           )
				(cons acTitleVertRightLineWeight      "acTitleVertRightLineWeight"      )
				(cons acTitleVertRightVisibility      "acTitleVertRightVisibility"      )
				(cons acVertCellMargin                "acVertCellMargin"                )
			)                                                                           
		)
		(setq AcCellContentLayout_enumValues
			(list
				(cons acCellContentLayoutFlow                  "acCellContentLayoutFlow"               )
				(cons acCellContentLayoutStackedHorizontal     "acCellContentLayoutStackedHorizontal"  )
				(cons acCellContentLayoutStackedVertical       "acCellContentLayoutStackedVertical"    )
			)
		)
		
		(setq AcCellContentType_enumValues
			(list
				(cons acCellContentTypeBlock     "acCellContentTypeBlock"      )
				(cons acCellContentTypeField     "acCellContentTypeField"      )
				(cons acCellContentTypeUnknown   "acCellContentTypeUnknown"    )
				(cons acCellContentTypeValue     "acCellContentTypeValue"      )
			)
		)
	)



	(progn ;; report on the table:
		(vlax-dump-object myTable)
		
		(foreach rowIndex (range (vla-get-rows myTable))
			(princ "row ") (princ rowIndex)(princ ":")(princ "\n")
			(princ "\t")(princ "(vla-GetRowType myTable rowIndex): ")(princ (cdr (assoc (vla-GetRowType myTable rowIndex) AcRowType_enumValues)))(princ "\n")

			(princ "\t")(princ "cells: ")(princ "\n")
			(foreach columnIndex (range (vla-get-columns myTable))
				(princ "\t\t")(princ "cell ")(princ "(")(princ rowIndex)(princ " ")(princ columnIndex)(princ ")")(princ ": ")                                                                               (princ "\n")


				;;figure out how many content items this table cell contains (content items are indexed starting from zero.  By default, every cell has zero content items.  The stuff that the user inserts into a cell with the UI tends to end up in content item zero.
				(setq maxContentIndexToCheck 100) ;; a guard to prevent an endless loop, in case the function does not ever throw an exception.
				;; step through contentIndices and call some function that needs a valid contentIndex until we hit an exception.
				(setq contentIndex 0)
				(setq numberOfContentItems 0)
				(while 
					(and 
						(< contentIndex maxContentIndexToCheck)
						(or
							; (not 
								; (vl-catch-all-error-p 
									; (setq result 
										; (vl-catch-all-apply 
											; ; 'vla-GetBlockTableRecordId2 (list myTable rowIndex columnIndex contentIndex) ;;returns zero without throwing exception on non-existent content items.
											; 'vla-GetValue (list myTable rowIndex columnIndex contentIndex)
										; )
									; )
								; )
							; ) 
							(/= (vlax-variant-type (vla-GetValue myTable rowIndex columnIndex contentIndex)) vlax-vbEmpty) ; vla-GetValue will return an empty variant in the case where the content contains a block reference.
							
							;; this is not too menaningful in testing for the existence of a content item because GetDataType2 says that the data type is acGeneral in the case where the content item does not exist.
							; (/=
								; (progn	
									; (vla-GetDataType2 myTable rowIndex columnIndex contentIndex 'dataType_out 'unitType_out)
									; dataType_out
								; )
								; acUnknownDataType
							; )
							
							(/= 
								(vla-GetBlockTableRecordId2 myTable rowIndex columnIndex contentIndex)
								0
							)
							
						)
					)
					
					; (princ "result: ")
					; (princ
						; (cdr 
							; (assoc (vlax-variant-type result) variantType_enumValues )
						; )
					; )
					; (princ result) 
					; (princ "\n")
					(plusPlus 'contentIndex)
					(plusPlus 'numberOfContentItems)
				)
				
				
				(princ "\t\t\t")(princ     "ContentLayout: "       ) (princ "\t") (princ (cdr (assoc (vla-GetContentLayout myTable rowIndex columnIndex) AcCellContentLayout_enumValues)))  (princ "\n")
				(princ "\t\t\t")(princ     "ContentType: "         ) (princ "\t") (princ (cdr (assoc (vla-GetContentType myTable rowIndex columnIndex) AcCellContentType_enumValues)))      (princ "\n")
				(princ "\t\t\t")(princ     "CellStyle: "           ) (princ "\t") (princ (vla-GetCellStyle myTable rowIndex columnIndex))                                                   (princ "\n")
				(princ "\t\t\t")(princ     "CellStyleOverrides: "  )(princ "\t")                                                 
				(princ 
					(mapcar	
						'(lambda (x) 
							; (list
								; x
								; (cdr (assoc x acTableStyleOverrides_enumValues))
							; )
							(cdr (assoc x acTableStyleOverrides_enumValues))
						)
						; (if 
							; (vl-catch-all-error-p  
								; (setq result 
									; (vl-catch-all-apply 
										; 'vla-GetCellStyleOverrides (list myTable rowIndex columnIndex)
									; )
								; )
							; )
							; (list )
							(if 
								(vl-catch-all-error-p  
									(setq result 
										(vl-catch-all-apply 
											'gc:VariantToLispData (list  (vla-GetCellStyleOverrides myTable rowIndex columnIndex))
										)
									)
								)
								(list )
								result
							)
						; )
					)
				)
				(princ "\n")
				
				
				(princ "\t\t\t")(princ "has ")(princ numberOfContentItems)(princ " content items:")(princ "\n")
				(foreach contentIndex (range numberOfContentItems)
					
					(setq content (vla-GetValue  myTable rowIndex columnIndex contentIndex))
					(setq contentValue 
						(vl-catch-all-apply 'vlax-variant-value (list content))
					) 
					(princ "\t\t\t\t")(princ "content ")(princ contentIndex)(princ ": ")
					(princ
						(cdr 
							(assoc 
								(vlax-variant-type content)
								(list
									(cons  vlax-vbEmpty        "vlax-vbEmpty"    )
									(cons  vlax-vbNul          "vlax-vbNul"      )
									(cons  vlax-vbInteger      "vlax-vbInteger"  )
									(cons  vlax-vbLong         "vlax-vbLong"     )
									(cons  vlax-vbSingle       "vlax-vbSingle"   )
									(cons  vlax-vbDouble       "vlax-vbDouble"   )
									(cons  vlax-vbString       "vlax-vbString"   )
									(cons  vlax-vbObject       "vlax-vbObject"   )
									(cons  vlax-vbBoolean      "vlax-vbBoolean"  )
									(cons  vlax-vbArray        "vlax-vbArray"    )
								)
							)
						)
					)
					(princ " ")
					(princ
						(cdr 
							(assoc 
								(progn	
									(vla-GetDataType2 myTable rowIndex columnIndex contentIndex 'dataType_out 'unitType_out)
									dataType_out
								)
								(list
									(cons  acBuffer          "acBuffer"            )
									(cons  acDate            "acDate"              )
									(cons  acDouble          "acDouble"            )
									(cons  acGeneral         "acGeneral"           )
									(cons  acLong            "acLong"              )
									(cons  acObjectId        "acObjectId"          )
									(cons  acPoint2d         "acPoint2d"           )
									(cons  acPoint3d         "acPoint3d"           )
									(cons  acResbuf          "acResbuf"            )
									(cons  acString          "acString"            )
									(cons  acUnknownDataType "acUnknownDataType"   )
								)
							)
						)
					)
					(princ " ")
					(if (/= 0 (vla-GetBlockTableRecordId2 myTable rowIndex columnIndex contentIndex))
						(progn
							(setq blockDefinition (vla-ObjectIDToObject (vla-get-Document myTable) (vla-GetBlockTableRecordId2 myTable rowIndex columnIndex contentIndex)))
							(princ (vla-get-ObjectName blockDefinition))(princ ": ")
							(princ (vla-get-Name blockDefinition))
							;;(vlax-dump-object blockDefinition)
						)
						(progn	
							(princ "content: ")(princ contentValue)
						)
					)
					
					(princ "\n")
					;(vla-GetBlockTableRecordId2 myTable rowIndex columnIndex)
				)
				
				
				; (setq contentId (vla-CreateContent myTable rowIndex columnIndex -1))
				; (princ "contentId: ")(princ contentId)
				; (vla-setTextString myTable 
					; rowIndex
					; columnIndex
					; contentId
					; (strcat "xxx" (itoa contentId))
				; )
				
				;(vla-DeleteContent myTable rowIndex columnIndex contentId)
				
				; (setq contentId (vla-CreateContent reportTable rowIndex columnIndex 3))
				; (vla-setTextString reportTable 
					; rowIndex
					; columnIndex
					; 1
					; (strcat "xxx" (itoa contentId))
				; )
				; (setq contentId (vla-CreateContent reportTable rowIndex columnIndex 16))
				; (vla-setTextString reportTable 
					; rowIndex
					; columnIndex
					; contentId
					; (strcat "xxx" (itoa contentId))
				; )
				
				; (setq contentId (vla-CreateContent reportTable rowIndex columnIndex 16))
				; (vla-setTextString reportTable 
					; rowIndex
					; columnIndex
					; contentId
					; (strcat "GetCellBackgroundColorNone" (if (= :vlax-true (vla-GetCellBackgroundColorNone reportTable rowIndex columnIndex)) ":vlax-true" ":vlax-false"))
				; )
				
				
				
			)
		)



	)
	(progn ;; set column widths
		(vla-SetColumnWidth myTable
			0 ;;columnIndex
			1.5 ;;width
		)

		(vla-SetColumnWidth myTable
			1 ;;columnIndex
			4 ;;width
		)
	)

	(progn ;; clear all cell and table style overrides, to be sure that we are not fighting any pre-existing overrides, and so that all the formatting is thatwhich is inheited from the TableStyle.
		(foreach rowIndex (range (vla-get-rows myTable))
			(foreach columnIndex (range (vla-get-columns myTable))
				(vla-RemoveAllOverrides myTable rowIndex columnIndex)
			)
		)
		(vla-ClearTableStyleOverrides myTable
			; ; 0: Deletes all table and cell overrides.
			; ; 1: Deletes all table overrides, but retains cell overrides.
			; ; 2: Deletes all cell overrides, but retains table overrides.
			0
		)
	)

	(progn ;; set the grid line visibility.
		;; the grid line visibility scheme is a bit tricky because some grid lines fall into more than gridLineType 
		;; (for instance a horizontal line between two rows counts as an acHorzInside AND an acHorzTop (for the row below the cell) AND an acHorzBottom (for the row above the cell) ).
		;; It is not obvious which of these categories takes precedence and how the visibility directives combine.
		;; AS a general rule, I try to set as few overrides as necessary to achieve the desired effect..
		;; I suspect that the 'top' and 'bottom' specifiers take precedence over the 'inside' specifier. I suspect that the order of precendence is top, bottom, inside.
		;; Therefore, if you want to apply a visibility setting for all inside horizontal lines, you must not have applied the opposite setting to 'top' or 'bottom' horizontal lines.
		;; with vertical lines, I think 'left' and 'right' apply to the row, not to the column.
		;; In fact, I think the assymetry between the vertical line visibility behavior and the horizontal line visibility behvaior is entirely explained by
		;; both vertical and horizontal directives as applying to rows rather than the table as a whole.
		;; Of course, the documentation mentions none of this, nor much of anything, for that matter.
		;; It is highly annoying that the horizontal and vertical situations are not symmetric.
		
		(if nil
			(progn 
				;;set all gridlines to be visible.
				(foreach rowIndex (range (vla-get-rows myTable))
					(foreach columnIndex (range (vla-get-columns myTable))
						(foreach gridLineType (mapcar 'car AcGridLineType_enumValues)
							(if (/= gridLineType acInvalidGridLine)
								(progn
									(vla-SetGridVisibility2 myTable rowIndex columnIndex
										gridLineType
										;;visible
										:vlax-true
									)
								)
							)
						)
					)
				)
			)
		)




		
		
		(if nil
			(progn
				;;hide all  grid lines in all row types
				(vla-SetGridVisibility myTable
					;; gridLineTypes
					(+
						(* 1 acHorzTop     )
						(* 1 acHorzInside  ) 
						(* 1 acHorzBottom  )
						(* 1 acVertLeft    )
						(* 1 acVertInside  )
						(* 1 acVertRight   )
					)

					;; rowTypes
					(+
						(* 1 acDataRow    )
						(* 1 acHeaderRow  )
						(* 1 acTitleRow   )
						(* 1 acUnknownRow )
					)

					;; visible (either :vlax-true or :vlax-false)
					:vlax-false
				)

				
				;;show  all grid lines in data rows:
				(vla-SetGridVisibility myTable
					;; gridLineTypes
					(+
						(* 0 acHorzTop     )
						(* 0 acHorzInside  ) 
						(* 1 acHorzBottom  )
						(* 1 acVertLeft    )
						(* 1 acVertInside  )
						(* 1 acVertRight   )
					)

					;; rowTypes
					(+
						(* 1 acDataRow    )
						(* 0 acHeaderRow  )
						(* 0 acTitleRow   )
						(* 0 acUnknownRow )
					)

					;; visible (either :vlax-true or :vlax-false)
					:vlax-false
				)


				;show some horizontal lines in header and title rows:
				(vla-SetGridVisibility myTable
					;; gridLineTypes
					(+
						(* 0 acHorzTop     )
						(* 1 acHorzInside  ) 
						(* 1 acHorzBottom  )
						(* 0 acVertLeft    )
						(* 0 acVertInside  )
						(* 0 acVertRight   )
					)

					;; rowTypes
					(+
						(* 0 acDataRow    )
						(* 1 acHeaderRow  )
						(* 1 acTitleRow   )
						(* 0 acUnknownRow )
					)

					;; visible (either :vlax-true or :vlax-false)
					:vlax-true
				)


				;;the table-wide  grid visibility settings serve as the default for new cells created (I think), and affect existing cells except when you have used SetGridVisibility2 to specifically set the grid visibility for a particular cell.
				;; Once the grid visibility has been set for a particular cell, there does not seem to be a way to remove the cell-specific setting so that the cell will once again inherit the table-wide grid visibility settings.
				;; Actually, I suspect that the cell-specific grid visibility settings (or at least the flag that specifies that a particular cell has cell-specific grid visibility settings) are stored in the cell's CellStyleOverrides list,
				;; which is accessible by the (poorly documented) function Table::GetCellStyleOverrides(nRow, nCol).  This function returns a variant array, each element of which is an integer.
				;; Table::RemoveAllOverrides() clears a particular cell's overrides list.
				;; It appears that the table-wide grid visibility settings (as set with Table::SetGridVisibility) are stored in the table-wide overrides list, which can be cleared with Table::ClearTableStyleOverrides(). (Where are cascading style sheets when you need them??)
			)
		)
		
		
		
		;;hide all grid lines for data header and title rows
		(vla-SetGridVisibility myTableStyle 
				;; rowTypes
				(+
					(* 1 acDataRow    )
					(* 1 acHeaderRow  )
					(* 1 acTitleRow   )
					(* 1 acUnknownRow )
				)

			;; gridLineTypes
			(+
				(* 1 acHorzTop     )
				(* 1 acHorzInside  ) 
				(* 1 acHorzBottom  )
				(* 1 acVertLeft    )
				(* 1 acVertInside  )
				(* 1 acVertRight   )
			)
			;; visible (either :vlax-true or :vlax-false)
			:vlax-false
		)



		(vla-SetGridVisibility2 myTableStyle 
			"Title" ;;name of cell stlye
			;; gridLineTypes
			(+
				(* 1 acHorzTop     )
				(* 1 acHorzInside  ) 
				(* 1 acHorzBottom  )
				(* 1 acVertLeft    )
				(* 1 acVertInside  )
				(* 1 acVertRight   )
			)
			;; visible (either :vlax-true or :vlax-false)
			:vlax-false
		)

		(vla-SetGridVisibility2 myTableStyle 
			"Title" ;;name of cell stlye
			;; gridLineTypes
			(+
				(* 0 acHorzTop     )
				(* 0 acHorzInside  ) 
				(* 1 acHorzBottom  )
				(* 0 acVertLeft    )
				(* 0 acVertInside  )
				(* 0 acVertRight   )
			)
			;; visible (either :vlax-true or :vlax-false)
			:vlax-true
		)


		;; hide all grid lines for cells of style description_cellStyle and symbol_cellStyle

		(vla-SetGridVisibility2 myTableStyle 
			"description_cellStyle" ;;name of cell stlye
			;; gridLineTypes
			(+
				(* 1 acHorzTop     )
				(* 1 acHorzInside  ) 
				(* 1 acHorzBottom  )
				(* 1 acVertLeft    )
				(* 1 acVertInside  )
				(* 1 acVertRight   )
			)
			;; visible (either :vlax-true or :vlax-false)
			:vlax-false
		)


		(vla-SetGridVisibility2 myTableStyle 
			"symbol_cellStyle" ;;name of cell stlye
			;; gridLineTypes
			(+
				(* 1 acHorzTop     )
				(* 1 acHorzInside  ) 
				(* 1 acHorzBottom  )
				(* 1 acVertLeft    )
				(* 1 acVertInside  )
				(* 1 acVertRight   )
			)
			;; visible (either :vlax-true or :vlax-false)
			:vlax-false
		)


	)



	; (vlax-for dictionary (vla-get-Dictionaries (vla-get-Document myTable))
		; ; strangely, the Dictionaries collection contains some items that are note dictionaries (i.e.  ObjectName != "AcDbDictionary" and do not have a 'Name' property.)
		; (if (= "AcDbDictionary" (vla-get-ObjectName dictionary))
			; (progn
				; (princ (vl-catch-all-apply 'vla-get-Name (list dictionary)))(princ "\n")
			; )
		; )
	; )


	(vlax-dump-object myTableStyle)


	; (setq result 
		; (vlax-make-variant 
			; (vlax-make-safearray
				; vlax-vbObject  
				; (cons 0 (- (vla-get-NumCellStyles myTableStyle) 1))
			; )
		; )
	; )
	; (vla-GetCellStyles myTableStyle result)
	; (princ (gc:VariantToLispData  result))(princ "\n")
	; (princ result)(princ "\n")
	; (princ)


	;; assign the cell styles.
	(foreach rowIndex (range (vla-get-rows myTable))
		(foreach columnIndex (range (vla-get-columns myTable))
			(if (= rowIndex 0)
				(progn
					(vla-SetCellStyle myTable rowIndex columnIndex "Title")  ;;this is necessary when the title is a merged row of cells, to force the cell style "Title" to apply to all cells in the row so that the bottom line will be visible across the etnire row.
				)
				(progn
					(if (= columnIndex 0)
						(progn
							(vla-SetCellStyle myTable rowIndex columnIndex "symbol_cellStyle")
						)
					)
					(if (= columnIndex 1)
						(progn
							(vla-SetCellStyle myTable rowIndex columnIndex "description_cellStyle")
						)
					)
				)
			)
		)
	)

	;; a hack to force the bottom border of the title row to be visible.
	(foreach rowIndex (list 0)
		(foreach columnIndex (range (vla-get-columns myTable))
			(vla-SetCellStyle myTable rowIndex columnIndex "Title")  ;;this is necessary when the title is a merged row of cells, to force the cell style "Title" to apply to all cells in the row so that the bottom line will be visible across the etnire row.		
			(vla-SetGridVisibility2 myTable 
				rowIndex
				columnIndex
				;; gridLineTypes
				(+
					(* 0 acHorzTop     )
					(* 0 acHorzInside  ) 
					(* 1 acHorzBottom  )
					(* 0 acVertLeft    )
					(* 0 acVertInside  )
					(* 0 acVertRight   )
				)
				;; visible (either :vlax-true or :vlax-false)
				:vlax-true
			)
		)
	)


	;;this will further enforce that the first row is the title: (this affects the value returned by Table::GetRowType() for row zero.
	(vla-put-TitleSuppressed myTableStyle :vlax-false)
	(vla-put-TitleSuppressed myTable :vlax-false)







	(vla-GenerateLayout myTable)
	; (vla-setTextString myTable 
		; 1 ;; row index
		; 1 ;; column index
		; 0 ;; content index
		; "\\pqr;Ahoy"
	; )

	;(vla-put-AllowManualHeights myTable :vlax-false)
	(vla-put-RowHeight myTable 0.000001) ; this causes the rows to collapse to the natural height determined by the contents.
)

(if legendTable 
	(progn 
		(princ "now processing legendTable.")(princ "\n")
		(processTable legendTable)
	)
)

(if abbreviationsTable 
	(progn
		(princ "now processing  abbreviationsTable.")(princ "\n")
		(processTable abbreviationsTable)
		
		;; a hack to fix the alignment in the abbreviations table
		(foreach rowIndex (range (vla-get-rows abbreviationsTable))
			(foreach columnIndex (range (vla-get-columns abbreviationsTable))
				(if (> rowIndex 0)
					(progn
						(if (= columnIndex 0)
							(progn
								(vla-SetCellAlignment abbreviationsTable rowIndex columnIndex acBottomRight)
							)
						)
						(if (= columnIndex 1)
							(progn
								(vla-SetCellAlignment abbreviationsTable rowIndex columnIndex acBottomLeft)
							)
						)
					)
				)
			)
		)
	)
)

(princ)
