;; Defines functions to get and set a 'level' property -- an integer value.
(regapp "levelTag")

(defun getLevel
	(
		arg
		/
		theEname
		theEntity
		returnValue
		thisApplicationName
		xData
		xDataGroupCodeForInteger
	)
	(setq thisApplicationName "levelTag")
	(setq xDataGroupCodeForInteger 1070)
	(COND 
		(
			(= (type arg) 'ENAME)
			(setq theEntity 
				(vlax-ename->vla-object arg)
			)
		)
		(
			(= (type arg) 'VLA-OBJECT)
			(setq theEntity 
				arg
			)
		)
		(
			T
			(princ "error: getLevel was passed an argument that was neither an ENAME nor a VLA-OBJECT.")
		)
	)
	(vla-GetXData theEntity thisApplicationName 'xDataType 'xDataValue)
	(setq xDataType (gc:VariantToLispData xDataType))
	(setq xDataValue (gc:VariantToLispData xDataValue))
	(setq xData 
		(mapcar
			'cons
			(gc:VariantToLispData xDataType)
			(gc:VariantToLispData xDataValue)
		)
	)
	; xData is in the same format that would be returned by (cdr (cadr (assoc -3 (entget <ename of the theEntity>)))),
	; namely, xData is a list of dotted pairs of the form (xDataGroupCode . value)
	;;; (princ (cadr (assoc -3 (entget (car (nentsel)) (list thisApplicationName)))))(princ)

	(if 
		(> (length xData) 0 ) ;; theEntity has some levelTag xdata
		(progn
			(setq returnValue
				(cdr
					(nth 0
						(vl-remove-if-not '(lambda (x) (= xDataGroupCodeForInteger (car x))) xData)
					)
				)
			)
		)
		(progn
			;in this case, selectedEntity does not have any existing xData (at least none that we care about)
			;(princ "no exisiting xData found.\n")
			(setq returnValue nil) 
		)
	)
	returnValue
)
;==============


(defun setLevel
	(
		arg
		newLevel
		/
		theEname
		theEntity
		returnValue
		newXData
		thisApplicationName
		xDataGroupCodeForInteger
	)
	(setq thisApplicationName "levelTag")
	(setq xDataGroupCodeForInteger 1070)
	(setq xDataGroupCodeForApplicationName 1001)
	(COND 
		(
			(= (type arg) 'ENAME)
			(setq theEntity 
				(vlax-ename->vla-object arg)
			)
		)
		(
			(= (type arg) 'VLA-OBJECT)
			(setq theEntity 
				arg
			)
		)
		(
			T
			(princ "error: setLevel was passed an argument that was neither an ENAME nor a VLA-OBJECT.")
		)
	)
	
	(if (not (or (= 'INT (type newLevel)) (not newLevel))) (progn (princ "setLevel expects to be passed an INT or nil.")))
	
	;; Initialize all the xdata values. Note that first data in the list should be
    ;; application name and first datatype code should be 1001
	
	(setq newXData                       
		(list (cons xDataGroupCodeForApplicationName thisApplicationName))
	)
	
	(if newLevel
		(setq newXData                       
			(append 
				newXData
				(list (cons xDataGroupCodeForInteger newLevel))
			)
		)
	)
	
	(gc:SetXdata theEntity newXData)
	(setq returnValue newLevel)
	returnValue
)
	
;========================



(defun C:getLevelOfNestedEntity
	(

	/
		ename
		entity
		entityType
		x
	)
	
	(setq x (nentsel "Click on the entity whose level you want to know.\n"))
	(setq ename (car x))
	(setq nestingDepth (length (last x)))
	(setq entity (entget ename))
	(setq entityType (cdr (assoc 0 entity)))
	
	(princ "\n")
	(princ 
		(strcat
			"type: " entityType "."  "\t" "\n"
			"nestingDepth: " (itoa nestingDepth) "."  "\t" "\n"
			"level: "
		)
	) 
	(princ (getLevel ename)) (princ "\n")
	(princ ".")
	(princ "\n")
	(princ)
)
;========


(defun C:setLevelOfNestedEntity
	(

		/
		ename
		entity
		entityType
		x
		originalLevel
		newLevel
		defaultNewLevel
	)
	
	(setq x (nentsel "Click on the object whose level you want to set.\n"))
	(setq ename (car x))
	(setq nestingDepth (length (last x)))
	(setq entity (entget ename))
	(setq entityType (cdr (assoc 0 entity)))
	(setq originalLevel (getLevel ename))
	
	(setq defaultNewLevel originalLevel)
	(setq newLevel (getstring T (strcat "What would you like the new level to be?  (enter an integer) <" (vl-princ-to-string defaultNewLevel) ">: ")))
	
	(if (= 0 (strlen newLevel)) 
		(progn
			(setq newLevel defaultNewLevel)
		)
		(progn
			(setq newLevel 
				(read newLevel)
			)
		)
	)
	
	;; at this point, newLevel is an integer or else is nil
	(progn
		(if newLevel (setLevel ename newLevel))
		(princ "Success! The level of the selected ")(princ entityType)(princ " is now " )(princ (getLevel ename))(princ ".  ")(princ "(it used to be ") (princ originalLevel) (princ ").")(princ "\n")
	)


	(princ)
)
;=====
